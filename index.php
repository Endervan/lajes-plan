<?php
	require_once("./class/Include.class.php"); 
	$obj_site = new Site();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="css/css.css" rel="stylesheet" type="text/css" />
<?php include("includes/head.php") ?>

<meta name="description" content="<?php echo $description?>" />
<meta name="keywords" content="lajesplan, sua obra" />
<title><?php echo $title ?></title>


<link rel="stylesheet" href="jquery/slider/css/global.css">
	
<?php /*?>	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script><?php */?>
<script src="jquery/slider/js/slides.min.jquery.js"></script>
<script>
		$(function(){
			$('#slides').slides({
				preload: true,
				preloadImage: 'jquery/slider/img/loading.gif',
				play: 2000,
				pause: 2000,
				hoverPause: true,
				animationStart: function(current){
					$('.caption').animate({
						bottom:0
					},100);
					if (window.console && console.log) {
						// example return of current slide number
						console.log('animationStart on slide: ', current);
					};
				},
				animationComplete: function(current){
					$('.caption').animate({
						bottom:0
					},200);
					if (window.console && console.log) {
						// example return of current slide number
						console.log('animationComplete on slide: ', current);
					};
				},
				slidesLoaded: function() {
					$('.caption').animate({
						bottom:0
					},200);
				}
			});
		});
	</script>

</head>

<body>
    <div id="corpo">
    <!--Topo-->
    	<div id="topo">
			<?php include("includes/topo.php") ?>
        </div>
    
     <!--Banner-->
     <div id="banner">
     	<?php include("banner/banner.php");?>
     </div>
     <!--Corpo-->
        <div id="conteiner">
        <?php
			$result_empresa = $obj_site->select_unico("tb_empresas","idempresa",4);// Busca o conteudo referente ao id 4 select_unico($nome_tabela, $id_tabela, $id)
	
		?>
        	<div id="a-empresa-home">
            	<?php
					if(!empty($result_empresa[imagem]) and file_exists("uploads/".$result_empresa[imagem])){
						$img = "uploads/".$result_empresa[imagem];
					}else{
						$img = "uploads/sem-imagem.jpg";
					}
				?>
            	<img src="<?php echo $img?>" alt="<?php Util::imprime($result_empresa[titulo])?>"/>
                <div class="texto-empresa-home">
                	<h1><?php Util::imprime($result_empresa[titulo])?></h1>
                    <p>
                   <?php Util::imprime($result_empresa[descricao],450)?>...
                    <a href="<?php echo Util::caminho_projeto()?>empresa" title="saiba mais">saiba mais &rsaquo;</a>
                    </p>
                </div>
                <a href="<?php echo Util::caminho_projeto()?>empresa" title="conheça a empresa" class="link-conheca"><span>conheça a empresa</span></a>
            </div>

            <div class="item-direita">
             <?php
				$result_empresa = $obj_site->select_unico("tb_empresas","idempresa",9);// Busca o conteudo referente ao id 6 select_unico($nome_tabela, $id_tabela, $id)
			?>
            	<h2><?php Util::imprime($result_empresa[titulo])?></h2>
                <p><?php Util::imprime(strip_tags($result_empresa[descricao]))?></p>
                  <a href="<?php echo Util::caminho_projeto()?>dicas" title="veja mais dicas" class="link-conheca"><span>veja mais dicas</span></a>
            </div>
             <div class="item-direita">
            <?php
				$result_empresa = $obj_site->select_unico("tb_empresas","idempresa",7);// Busca o conteudo referente ao id 7 select_unico($nome_tabela, $id_tabela, $id)
			?>
            	<h2><?php Util::imprime($result_empresa[titulo])?></h2>
                <p><?php Util::imprime(strip_tags($result_empresa[descricao]))?></p>
                  <a href="<?php echo Util::caminho_projeto()?>contato" title="entre em contato" class="link-conheca"><span>entre em contato</span></a>
            </div>
            <div class="clear"></div>
                <div id="fornecedores-home">
            <?php
				$result_empresa = $obj_site->select_unico("tb_empresas","idempresa",8);// Busca o conteudo referente ao id 8 select_unico($nome_tabela, $id_tabela, $id)
			?>
                    <h1><?php Util::imprime($result_empresa[titulo])?></h1>
                    <p><?php Util::imprime($result_empresa[descricao])?></p>
                    <!--thumbs fornecedores-->
                    <ul class="imgs-fornecedores">
                    <?php
						$result_fornecedores = $obj_site->select("tb_fornecedores"," ORDER BY RAND() LIMIT 5");// Busca o conteudo referente ao id 4 select_unico($nome_tabela, $id_tabela, $id)
						while($arr_fornecedores = mysql_fetch_array($result_fornecedores)){
							if(!empty($arr_fornecedores[imagem_capa]) and file_exists("uploads/".$arr_fornecedores[imagem_capa])){
					?>
                        <li><img src="<?php echo Util::caminho_projeto();?>uploads/tumb_<?php echo $arr_fornecedores[imagem_capa]?>" alt="<?php Util::imprime($arr_fornecedores[titulo])?>" /></li>
                   	<?php
							}
						}
					?>
                    </ul>
                    <a href="<?php echo Util::caminho_projeto()?>fornecedores" title="veja todos os fornecedores" class="link-conheca"><span>veja todos os fornecedores</span></a>
                </div>
        </div>
        
       <!--Rodape-->
      <div	id="rodape">
			<?php include("includes/rodape.php"); ?>
      </div>
    </div>
</body>
</html>