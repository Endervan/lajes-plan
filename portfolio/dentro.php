<?php
	require_once("../class/Include.class.php"); 
	$obj_site = new Site();
	
	require_once("../class/Resumo.class.php"); 
	$obj_resumo = new resumo();
	
	/*Recupera os parametros da url*/
	$parametros = explode("-",addslashes($_GET[get1]));
	$id = $parametros[0];
	
	$portfolio = $obj_site->select_unico("tb_portfolio", "idportfolio", $id); //Busca o conteudo de acordo com o id passado
	
	$categoria = $obj_site->select_unico("tb_categorias", "idcategoria", $portfolio[id_categoria]);
	
	$description = $obj_resumo->str_truncate($portfolio[descricao],150);
	$texto = html_entity_decode($portfolio[titulo]);
	$texto = strip_tags($texto);
	$texto = stripslashes($texto);
	$texto = utf8_encode($texto);
	
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<?php include("../includes/head.php") ?>
<?php $title = $texto.' - Lajesplan :: Produtos'; ?>
<meta name="description" content="<?php echo $description?>" />
<meta name="keywords" content="lajesplan, sua obra" />
<title><?php echo $title ?></title>

<!--Troca a foto que esta sendo visualizada no slider-->
<script type="text/javascript"> 
function mudaFoto(id)
{
    var elemento = "#container-infos-galeria-grande-"+id;
    $("#container-infos-galeria-grande div").stop(true, true).fadeOut(500).hide(500);
    $(elemento).stop(true, true).fadeIn(1000).show(500);
}
</script>
</head>

<body class="bg-interna">
    <div id="corpo">
    <!--Topo-->
    	<div id="topo">
			<?php include("../includes/topo.php") ?>
        </div>
    
     <!--Banner-->
     <div id="banner-interna">
   	   <div class="titulo-pagina"><h1><?php echo $local_pagina; ?></h1></div>
       <div class="onde-estou">
        	<a href="<?php echo Util::caminho_projeto();?>">home</a> <span>></span> <a href="<?php echo Util::caminho_projeto();?>portfolio"> <?php echo $local_pagina; ?></a> <span>> </span> <a href="<?php echo Util::caminho_projeto();?>portfolio/?categoria=<?php Util::imprime($categoria[0]); ?>"> <?php Util::imprime($categoria[titulo]); ?></a> <span>> <strong><?php Util::imprime($portfolio[titulo]);?></strong></span>
        </div>
     </div>
     <!--Corpo-->
        <div id="conteiner-interna">    
            <h2><?php Util::imprime($portfolio[titulo]);?></h2>
 <?php /*?>INICIO da div das fotos<?php */?>
 <?php
 	 $result = $obj_site->select("tb_imagens_portfolios", "AND id_portfolio = '".$portfolio[0]."'");
	 	if(mysql_num_rows($result)>0){
 ?>
<div id="fotos">
    <div id="container-infos-galeria-grande">
		<?php   
			while($row = mysql_fetch_array($result)){
				$i++;
				$tumb[] = $row[imagem]; 
        ?>  
            <div id="container-infos-galeria-grande-<?php echo $i ?>">
                <a href="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $row[imagem] ?>" class="vlightbox1">
                	<img src="../resize.php?arquivo=uploads/<?php echo $row[imagem] ?>&altura=320&largura=410&tipo=crop"  />
                </a>
            </div>
        <?php
        	}
        ?>  
    </div>
    <div id="lista-videos-imagens-interna-ul">
        <div id="lista-videos-imagens-interna-ul-listagem">                                
            <ul>
                <?php
                    if(count($tumb) > 0){
                        $i = 0;
                        $b = 1;
                        
                        foreach($tumb as $tumb){
                            $i++;
                ?>
                    <li>
                        <div>
                            <a href="javascript:void(0)" onclick="mudaFoto(<?php echo $i; ?>)">
                                <div style="overflow:hidden;">
                                     <img src="../resize.php?arquivo=uploads/tumb_<?php echo $tumb; ?>&altura=80&largura=89&tipo=" alt="" width="100%" height="100%"  />
                                </div>
                            </a>
                        </div>
                    </li>
                <?php
                        }
                        
                    }
                ?>
            </ul>
        </div>
        
            <div id="lista_btn_voltar">
                <a href="javascript:void(0);" title="Voltar" id="btn_voltar_slider_inst">
                    <img src="../imgs/slider_voltar.jpg" alt="Voltar" />
                </a>
            </div>
            <div id="lista_btn_avancar">
                <a href="javascript:void(0);" title="Avançar" id="btn_avancar_slider_inst">
                    <img src="../imgs/slider_avancar.jpg" alt="Avançar" />
                </a>
            </div>
    </div>
</div>
<?php 
		}
?>
<?php /*?>Fim da div das fotos<?php */?>
            
            <p><?php Util::imprime($portfolio[descricao]);?></p>
               
           <div class="clear"></div>
        </div>
        
       <!--Rodape-->
      <div	id="rodape">
			<?php include("../includes/rodape.php"); ?>
      </div>
    </div>
</body>
</html>