<?php
	require_once("../class/Include.class.php"); 
	$obj_site = new Site();
	
	/*Recupera os parametros*/
	
	if(isset($_GET[categoria])){
		$complemento .= " AND id_categoria = '".addslashes($_GET[categoria])."'";
		
		//busca a categoria
		$categoria = $obj_site->select_unico("tb_categorias", "idcategoria", addslashes($_GET[categoria]));
		
		$nome_filtro = $categoria[titulo];
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<?php include("../includes/head.php") ?>

<meta name="description" content="<?php echo $description?>" />
<meta name="keywords" content="lajesplan, sua obra" />
<title><?php echo $title ?></title>
</head>

<body class="bg-interna">
    <div id="corpo">
    <!--Topo-->
    	<div id="topo">
			<?php include("../includes/topo.php") ?>
        </div>
    
     <!--Banner-->
     <div id="banner-interna">
   	    <?php include("../includes/onde-estou.php"); ?> 
     </div>
     <!--Corpo-->
        <div id="conteiner-interna">    
               <ul id="lista-portfolio">
				   <?php
                        $result = $obj_site->select("tb_portfolio"," $complemento ORDER BY titulo ASC");
                    while($arr = mysql_fetch_array($result)){
                        
                        $categoria = Util::troca_value_nome($arr[id_categoria], "tb_categorias", "idcategoria", "ativo");//busca se a categoria esta ativo ou não
                        
                        
                        if($categoria=="SIM"){
                   ?>
                   
                        <li>
                            <p>
                                <?php
                                    if(!empty($arr[imagem]) and file_exists("../uploads/".$arr[imagem])){
                                ?>
                                <div class="thumb-img">
                                    <a href="<?php echo Util::caminho_projeto()?>portfolio/<?php echo $obj_site->url($arr[0],$arr[titulo]) ?>" title="<?php Util::imprime($arr[titulo])?>"></a>
                                    <img src="<?php echo Util::caminho_projeto()?>uploads/tumb_<?php Util::imprime($arr[imagem])?>" alt="<?php Util::imprime($arr[titulo])?>" />
                                </div>
                                <?php
                                    }
                                ?>
                                <?php /*?><h2><?php Util::imprime($arr[titulo])?></h2>
                                <?php Util::imprime($arr[descricao])?><?php */?>
                            </p>
                        </li>
                        
                    <?php
                        }
                    }
                    ?>
               </ul>
               
           <div class="clear"></div>
        </div>
        
       <!--Rodape-->
      <div	id="rodape">
			<?php include("../includes/rodape.php"); ?>
      </div>
    </div>
</body>
</html>