<?php
	require_once("../class/Include.class.php"); 
	$obj_site = new Site();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<?php include("../includes/head.php") ?>

<meta name="description" content="<?php echo $description?>" />
<meta name="keywords" content="lajesplan, sua obra" />
<title><?php echo $title ?></title>


</head>

<body class="bg-interna">
    <div id="corpo">
    <!--Topo-->
    	<div id="topo">
			<?php include("../includes/topo.php") ?>
        </div>
    
     <!--Banner-->
     <div id="banner-interna">
   	    <?php include("../includes/onde-estou.php"); ?>
     </div>
     <!--Corpo-->
        <div id="conteiner-interna">
        	<?php 
				$result = $obj_site->select("tb_empresas"," AND idempresa IN (4,5) ORDER BY titulo ASC");
				while($arr = mysql_fetch_array($result)){
			?>          
            	 <h2><?php Util::imprime($arr[titulo]); ?></h2>
                 <?php if(!empty($arr[imagem]) and file_exists("../uploads/".$arr[imagem])){ ?>
            		<img src="<?php echo Util::caminho_projeto();?>/uploads/<?php Util::imprime($arr[imagem])?>" alt="<?php Util::imprime($arr[titulo]); ?>" />
            	<?php	} ?>
                 <p><?php Util::imprime($arr[descricao]); ?></p>
            <?php
				}
			?>
          <div class="clear"></div>
        </div>
        
       <!--Rodape-->
      <div	id="rodape">
			<?php include("../includes/rodape.php"); ?>
      </div>
    </div>
</body>
</html>