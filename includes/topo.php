<div id="logo">
    <a href="<?php echo Util::caminho_projeto() ?>"><img src="<?php echo Util::caminho_projeto() ?>imgs/logo.png" alt="Lajesplan" /></a>
    <span>CREA 1565/DF</span>
</div>
<div id="orcamento-topo">
    <ul>
        <li><a href="<?php echo Util::caminho_projeto() ?>contato" title="Solicite um orçamento 61 3225.8084 - 3381-2492"><img src="<?php echo Util::caminho_projeto() ?>imgs/telefone-topo.png" alt="Solicite um orçamento 61 3225.8084 - 3381-2492" /></a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>contato" title="Contato"><img src="<?php echo Util::caminho_projeto() ?>imgs/ico-email.png" alt="Contato" /></a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>contato" title="Localização"><img src="<?php echo Util::caminho_projeto() ?>imgs/ico-localizacao.png" alt="Localização" /></a></li>
    </ul>
</div>
<div id="menu">
    <ul>
        <li><a href="<?php echo Util::caminho_projeto() ?>" title="home" class="link-menu<?php echo $class1?>">home</a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>empresa" title="a empresa" class="link-menu<?php echo $class2?>">a empresa</a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>portfolio" title="portfólio" class="link-menu<?php echo $class3?>">portfólio</a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>produtos" title="produtos" class="link-menu<?php echo $class4?>">produtos</a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>fornecedores" title="fornecedores" class="link-menu<?php echo $class5?>">fornecedores</a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>parceiros" title="parceiros" class="link-menu<?php echo $class7?>">parceiros</a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>contato" title="contatos" class="link-menu<?php echo $class6?>">contatos</a></li>
    </ul>
</div>

    