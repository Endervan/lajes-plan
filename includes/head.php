<?php
if(Util::mobile_device_detect(true,true,true,true,true,true,Util::caminho_projeto().'/mobile/',false))
{
    header('Location: '.Util::caminho_projeto().'/mobile/');
};


header("Pragma: no-cache");
header("Cache: no-cache");
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
?>
<?php
/*Url amigavel*/
 function UrlAmigave() {
	 $pageURL = 'http';
		 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
			$pageURL .= "://";
		 if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		 } else {
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		 }
	 return $pageURL;
 }
 
?>

<!--	====================================================================================================	 -->	
<!--	MENU SELECT	 -->
<!--	====================================================================================================	 -->
<?php

$description = 'LAJESPLAN Brasília - DF é uma empresa calcada na inovação com filosofias projetadas a frente de seu contemporâneo. Seu vasto portfólio é composto por obras com lajes calculadas e executadas com vãos de até 13 metros e cargas acidentais de até 3000 kgf/m2 com características unidirecional e bidirecional tanto no sistema nervurado treliçado como painel maciço treliçado.';
$title = "Lajesplan Brasília - DF, Lajes Convencionais, Bi-dimensionais, Tri-dimensionais, Pré-fabricas, Treliçadas, Isopor, Postes de Concreto.";
	


//	ESCOLHO AS CLASSES DO MENU
switch (Util::nome_diretorio())
{	
	case "";
		$class1	= "-selected";
		$class2 = "";
		$class3 = "";
		$class4  = "";
		$class5 = "";
		$class6 = "";
		$class7 = "";
		$local_pagina = "";
		//$description = 'Lajesplan A forma mais moderna e inteligente de garantir a tranquilidade que você necessita em sua obra';
		//$title = "Lajesplan Brasília - DF, Lajes Convencionais, Bi-dimensionais, Tri-dimensionais, Pré-fabricas, Treliçadas, Isopor, Postes de Concreto.";
	break;
	case "/empresa";
		$class1	= "";
		$class2 = "-selected";
		$class3 = "";
		$class4  = "";
		$class5 = "";
		$class6 = "";
		$class7 = "";
		$local_pagina = "a empresa";
		//$description = 'Lajesplan Empreendimentos Imobiliários';
		//$title = "Lajesplan Brasília - DF, Lajes Convencionais, Bi-dimensionais, Tri-dimensionais, Pré-fabricas, Treliçadas, Isopor, Postes de Concreto.";
	break;
	case "/portfolio";
		$class1	= "";
		$class2 = "";
		$class3 = "-selected";
		$class4  = "";
		$class5 = "";
		$class6 = "";
		$class7 = "";
		$local_pagina = "portfólio";
		//$description = 'Lajesplan Empreendimentos Imobiliários';
		//$title = "Lajesplan Brasília - DF, Lajes Convencionais, Bi-dimensionais, Tri-dimensionais, Pré-fabricas, Treliçadas, Isopor, Postes de Concreto.";
	break;
	case "/produtos";
		$class1	= "";
		$class2 = "";
		$class3 = "";
		$class4  = "-selected";
		$class5 = "";
		$class6 = "";
		$class7 = "";
		$local_pagina = "produtos";
		//$description = 'Veja quais são os nossos produtos';
		//$title = "Lajesplan Brasília - DF, Lajes Convencionais, Bi-dimensionais, Tri-dimensionais, Pré-fabricas, Treliçadas, Isopor, Postes de Concreto.";
	break;
	case "/fornecedores";
		$class1	= "";
		$class2 = "";
		$class3 = "";
		$class4  = "";
		$class5 = "-selected";
		$class6 = "";
		$class7 = "";
		$local_pagina = "fornecedores";
		//$description = 'Conheça os nossos fornecedores.';
		//$title = "Lajesplan :: Fornecedores";
	break;
	case "/contato";
		$class1	= "";
		$class2 = "";
		$class3 = "";
		$class4  = "";
		$class5 = "";
		$class6 = "-selected";
		$class7 = "";
		$local_pagina = "contatos";
		//$description = 'Entre em contato conosco para mais informações.';
		//$title = "Lajesplan :: Contatos";
	break;
	case "/dicas";
		$class1	= "";
		$class2 = "";
		$class3 = "";
		$class4  = "";
		$class5 = "";
		$class6 = "";
		$class7 = "";
		$local_pagina = "dicas";
		//$description = 'As melhores dicas.';
		//$title = "Lajesplan :: Dicas";
	break;
	case "/parceiros";
		$class1	= "";
		$class2 = "";
		$class3 = "";
		$class4  = "";
		$class5 = "";
		$class6 = "";
		$class7 = "-selected";
		$local_pagina = "parceiros";
		//$description = 'Nossos parceiros.';
		//$title = "Lajesplan :: Parceiros";
	break;
	default;
		$class1	= "-selected";
		$class2 = "";
		$class3 = "";
		$class4  = "";
		$class5 = "";
		$class6 = "";
		$local_pagina = "";
		//$description = 'Lajesplan A forma mais moderna e inteligente de garantir a tranquilidade que você necessita em sua obra';
		//$title = "Lajesplan - Pré-Moldados Ind Com e Construção";
	break;
}
?>
<!--Meta Tags-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="revisit-after" content="7 Days" />
<meta name="language" content="pt-br" /> 
<meta name="robots" content="all" /> 
<meta name="rating" content="general" /> 
<meta name="copyright" content="Copyright Tekan www.tekan.com.br" /> 
<link rel="canonical" href="<?php echo UrlAmigave();?>" />

<!--Favicon-->
<link rel="shortcut icon" href="<?php echo Util::caminho_projeto() ?>imgs/favicon.ico" type="image/x-icon" />

<!--CSS-->
<link href="<?php echo Util::caminho_projeto() ?>css/css.css" rel="stylesheet" media="all" type="text/css" />
<!--[if lt IE 8]>        
    <link href="<?php echo Util::caminho_projeto(); ?>css/cssIE7.css" rel="stylesheet" type="text/css" /> 
<![endif]-->

<!--FONTS-->
<link href='http://fonts.googleapis.com/css?family=PT+Serif:400,400italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>

<!--	====================================================================================================	 -->	
<!--	JQUERY	 -->
<!--	====================================================================================================	 -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto(); ?>jquery/jquery-1.7.2.min.js"></script>


<!--	====================================================================================================	 -->	
<!--	GOOGLE +	 -->
<!--	====================================================================================================	 -->
<script type="text/javascript" src="http://apis.google.com/js/plusone.js"></script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36207121-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>


<link rel="stylesheet" href="<?php echo Util::caminho_projeto(); ?>/VisualLightBox/jquery/engine/css/vlightbox1.css" type="text/css" />
<link rel="stylesheet" href="<?php echo Util::caminho_projeto(); ?>/jquery/VisualLightBox/engine/css/visuallightbox.css" type="text/css" media="screen" />
<script src="<?php echo Util::caminho_projeto(); ?>/jquery/VisualLightBox/engine/js/visuallightbox.js" type="text/javascript"></script>
<script src="<?php echo Util::caminho_projeto(); ?>/jquery/VisualLightBox/engine/js/vlbdata1.js" type="text/javascript"></script>

<!--    ====================================================================================================     -->    
<!--    SLIDER   -->
<!--    ====================================================================================================     -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto(); ?>/jquery/jquery-infinite-carousel/jquery.infinite-carousel.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#lista-videos-imagens-interna-ul-listagem').carousel('#btn_voltar_slider_inst', '#btn_avancar_slider_inst');
    });
</script>
