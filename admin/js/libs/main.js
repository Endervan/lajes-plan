/*
 * jQuery upload - main file
 */
$(document).ready(function(){
	$("#upload-container").jqswfupload({
		onUploadStart: function(file) {
			$('#debug-bar').append('<p>Upload Started: '+file.name+'</p>');
		},
		onFileSuccess: function(file,data,response) {
			$('#debug-bar').append('<p>File success: '+file.name+'</p><pre>'+data+'</pre>');
		},
		onFileError: function(file,code,message){
			$('#debug-bar').append('<p>File Error: '+file.name+'</p><pre>'+message+'</pre>');
		},
		totalSize: '100 kb',
		uploadUrl: upload_uri + 'upload.php',
		// The post sent to server in the same request as the file object
		post_params : {
			'type' : 'swfupload',
			'test' : 'val'
		}
	});
});

