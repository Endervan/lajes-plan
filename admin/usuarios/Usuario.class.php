<?php
require_once("../../class/Include.class.php");




class Usuario extends Dao
{
	

		public $nome_tabela = "tb_usuarios";
		public $id_tabela = "idusuario";
	
	
	
		#-------------------------------------------------------------------------------------------------#
		#	CAMPOS DO FORMULARIO
		#-------------------------------------------------------------------------------------------------#
		public function campos()
		{
			$campos = array(
							array(
								  'nome_campo_form'		=>	'nome',				//	NOME DO CAMPO NO FORMULARIO			
								  'obr'					=>	's',					//	INFORME SE O CAMPO � OBRIGATORIO
								  'msgerros'			=>	'Informe o nome',		//	MENSAGEM DE OBRIGATORIEDADE
								  'tipo'				=>	'texto',				//	TIPO DE DADOS DO CAMPO (texto, moeda, data, telefone, cep)
								  'alinhamento'			=>	'left',					//	ALINHAMENTO DO TEXTO
								  'link'				=>	's',					//	CRIA UM LINK PARA ORDENACAO
								  'nome_coluna'			=>	'Nome',				//	NOME QUE SER� EXIBIDO NA COLUNA
								  'exibir_listagem'		=>	's'						//	INFORME SE DEVERA SER EXIBIDO NA LISTAGEM
								  ),
							array(
								  'nome_campo_form'		=>	'email',				//	NOME DO CAMPO NO FORMULARIO			
								  'obr'					=>	's',					//	INFORME SE O CAMPO � OBRIGATORIO
								  'msgerros'			=>	'Informe o email',		//	MENSAGEM DE OBRIGATORIEDADE
								  'tipo'				=>	'texto',				//	TIPO DE DADOS DO CAMPO (texto, moeda, data, telefone, cep)
								  'alinhamento'			=>	'left',					//	ALINHAMENTO DO TEXTO
								  'link'				=>	'n',					//	CRIA UM LINK PARA ORDENACAO
								  'nome_coluna'			=>	'Email',				//	NOME QUE SER� EXIBIDO NA COLUNA
								  'exibir_listagem'		=>	'n'						//	INFORME SE DEVERA SER EXIBIDO NA LISTAGEM
								  ),
							array(
								  'nome_campo_form'		=>	'senha',				//	NOME DO CAMPO NO FORMULARIO			
								  'obr'					=>	's',					//	INFORME SE O CAMPO � OBRIGATORIO
								  'msgerros'			=>	'Informe a senha',		//	MENSAGEM DE OBRIGATORIEDADE
								  'tipo'				=>	'senha',				//	TIPO DE DADOS DO CAMPO (texto, moeda, data, telefone, cep)
								  'alinhamento'			=>	'left',					//	ALINHAMENTO DO TEXTO
								  'link'				=>	'n',					//	CRIA UM LINK PARA ORDENACAO
								  'nome_coluna'			=>	'TARGET',				//	NOME QUE SER� EXIBIDO NA COLUNA
								  'exibir_listagem'		=>	'n'						//	INFORME SE DEVERA SER EXIBIDO NA LISTAGEM
								  )
							);
			return $campos;
		}
	
		
		
		#-------------------------------------------------------------------------------------------------#
		#	CADASTRA OS DADOS
		#-------------------------------------------------------------------------------------------------#
		public function cadastrar()
		{				
			//	VERIFICO SE NAO HOUVE ERRO NO FORMULARIO
			if($this->get_dados_formulario() == false)
			{									
				//	VERIFICO SE NAO EXISTE O EMAIL NO BANCO
				if($this->verifica_usuario($_SESSION[dados][email]) == 0)
				{
					//	CADASTRO OS DADOS
					$this->set_cadastro_dados();
					
					//	MATA AS VARIAVEIS DE SESSAO
					$this->mata_session();
						
					//	MSG DE SUCESSO
					Util::script_msg("Cadastro efetuado com sucesso");
		
					// 	REDIRECIONA PARA A PAGINA
					Util::script_location("cadastro.php");
				}
				else
				{
					//	MSG DE SUCESSO
					Util::script_msg("Este email j� est� cadastro.");
		
					// 	REDIRECIONA PARA A PAGINA
					Util::script_location("cadastro.php");
				}
				
				
			}
		}
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	ALTERA OS DADOS
		#-------------------------------------------------------------------------------------------------#
		public function verifica_usuario($email)
		{
			$sql = "SELECT * FROM ". $this->nome_tabela ." WHERE email = '$email'";
			return mysql_num_rows(parent::executaSQL($sql));
		}
		
		
		#-------------------------------------------------------------------------------------------------#
		#	ALTERA OS DADOS
		#-------------------------------------------------------------------------------------------------#
		public function verifica_usuario_alteracao($email, $idusuario)
		{
			$sql = "SELECT * FROM ". $this->nome_tabela ." WHERE email = '$email' AND idusuario <> '$idusuario'";
			return mysql_num_rows(parent::executaSQL($sql));
		}
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	ALTERA OS DADOS
		#-------------------------------------------------------------------------------------------------#
		public function alterar($id)
		{	
			//	VERIFICO SE NAO HOUVE ERRO NO FORMULARIO
			if($this->get_dados_formulario() == false)
			{					
				
				//	VERIFICO SE NAO EXISTE O EMAIL NO BANCO
				if($this->verifica_usuario_alteracao($_SESSION[dados][email], $id) == 0)
				{
					//	ALTERA OS DADOS
					$this->set_altera_dados($id);
									
					//	MATA AS VARIAVEIS DE SESSAO
					$this->mata_session();
					
					//	MSG DE SUCESSO
					Util::script_msg("Alteracao efetuada com sucesso");
		
					// 	REDIRECIONA PARA A PAGINA
					Util::script_location("index.php");
				}
				else
				{
					//	MSG DE SUCESSO
					Util::script_msg("Esse email j� est� sendo utilizado por outro usu�rio.");
		
					// 	REDIRECIONA PARA A PAGINA
					return $_SESSION[dados];
				}
			}
			else
			{
				//	RETORNO OS DADOS DO FORMUL�RIO
				$obj_formulario = new Formulario($this->campos());
				return $obj_formulario->get_dados_formulario();
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	ALTERO OS DADOS DA TABELA
		#-------------------------------------------------------------------------------------------------#
		private function atualiza_nome_imagem_tabela($id, $coluna, $nome_imagem)
		{			
			//	ATUALIZO OS DADOS NA TABELA
			$sql = "UPDATE ". $this->nome_tabela ." SET $coluna = '$nome_imagem' WHERE ". $this->id_tabela ." = '$id' ";
			parent::executaSQL($sql);
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	BUSCO OS DADOS DO FORMULARIO
		#-------------------------------------------------------------------------------------------------#
		private function get_dados_formulario()
		{
	
			//	BUSCO OS DADOS DO FORMUL�RIO
			$obj_formulario = new Formulario($this->campos());
	
			//	VERIFICO SE RETORNOU ALGUM ERRO NO OBJETO, SEN�O CADASTRO OS DADOS NO BANCO
			if ($obj_formulario->get_erros() != 1)
			{
				$obj_formulario->get_msg_erros();
				return true;
			}
			else
			{
				// PEGO OS CAMPOS
				$_SESSION[campos] = $obj_formulario->get_campos();
	
				// PEGO OS DADOS
				$_SESSION[dados] = $obj_formulario->get_dados_formulario();
			}
		}
		
		
		
		
		
		
		
		
	
		
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	CADASTRO NA TABELA
		#-------------------------------------------------------------------------------------------------#
		private function set_cadastro_dados()
		{
			if (parent::executaINSERT($this->nome_tabela, $_SESSION[campos], $_SESSION[dados]) != 0)
			{
				$this->get_msg_erro();
			}
		}
	


		#-------------------------------------------------------------------------------------------------#
		#	ALTERO OS DADOS DA TABELA
		#-------------------------------------------------------------------------------------------------#
		private function set_altera_dados($id)
		{			
			//	ATUALIZO OS DADOS NA TABELA
			if (parent::executaALTERACAO($this->nome_tabela, $_SESSION[campos], $_SESSION[dados], $this->id_tabela, $id) != 0)
			{
				$this->get_msg_erro();
			}
		}
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	MATO AS VARIAVEIS DE SESS�O DA CLASSE
		#-------------------------------------------------------------------------------------------------#
		private function mata_session()
		{
			unset($_SESSION['campos']);
			unset($_SESSION['dados']);
		}
		
		
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	BUSCO OS DADOS DA TABELA
		#-------------------------------------------------------------------------------------------------#
		public function get_dados_tabela($id)
		{					
			//	BUSCO OS DADOS
			$sql = "SELECT * FROM ". $this->nome_tabela ." WHERE ". $this->id_tabela ." = '$id' ";
			$result = parent::executaSQL($sql);
			return mysql_fetch_array($result);
		}
		
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	EXCLUSAO
		#-------------------------------------------------------------------------------------------------#
		public function excluir($id)
		{
				$sql = "DELETE FROM ". $this->nome_tabela ." WHERE ". $this->id_tabela ."= '$id'";
				parent::executaSQL($sql);
				Util::script_msg("Item exclu�do com sucesso.");
		}
		
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	DESATIVA O PRODUTO
		#-------------------------------------------------------------------------------------------------#
		public function ativar_desativar($id, $acao)
		{			
			//	ATUALIZO OS DADOS NA TABELA
			$sql = "UPDATE ". $this->nome_tabela ." SET ATIVO = '$acao' WHERE ". $this->id_tabela ." = '$id' ";
			parent::executaSQL($sql);
			
			
			switch($acao)
			{
				case "SIM":
					Util::script_msg("Item ativado com sucesso.");
				break;
				case "NAO":
					Util::script_msg("Item desativado com sucesso.");
				break;
			}
			
		}
		
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	DESATIVA O PRODUTO
		#-------------------------------------------------------------------------------------------------#
		public function atualiza_ordem_registro($id, $ordem)
		{
			$sql = "UPDATE ". $this->nome_tabela ." SET ORDEM = '$ordem' WHERE ". $this->id_tabela ." = '$id' ";
			parent::executaSQL($sql);
		}
		
		
		
	
	
	
}




















?>












