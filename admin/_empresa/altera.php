<?php
require_once("../../class/Include.class.php");
require_once("Empresa.class.php");
$obj_empresas = new Empresa();
$caminho_projeto = Util::caminho_projeto();


$id = base64_decode($_GET[id]);


//	VERIFICO SE PARA EFETUAR O CADASTRO
if(isset($_POST[btn_cadastrar]))
{
	$obj_empresas->alterar($id);
}
else
{
	$row = $obj_empresas->get_dados_tabela($id);
}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="language" content="pt-br" /> 
<link href="../estilo/estilo.css" rel="stylesheet" type="text/css" />



<!------------------------------------------------------------------------------------------------------------------->
<!--	JSCRIPT	-->
<!------------------------------------------------------------------------------------------------------------------->
<?php
	$obj_jquery = new Biblioteca_Jquery();	
?>








<title>Administração do site</title>
</head>

<body>








<!-- Começa -->
<div id="geral">
	<!-- Div para topo -->
	<div id="topo"></div>
    
	<!-- Div para linha no bg -->
	<div id="linhaMenu">
		<div id="cabecalho">Gerenciando </div>
		<div id="sair"><a href="../logout.php"><img src="../img/sair.png" border="0" /></a> </div>
	</div>
    
	<!-- Div conteúdo -->
	<div id="conteudo">

	  
      <!---------------------------------------------------------------------------------------------------------------->
      <!-- Div menu lateral -->
      <!---------------------------------------------------------------------------------------------------------------->
		<div id="menu_left">
        	<div class="cabecalho_menu_left"></div>
            	<div id="menu_left_dentro">
                    <ul>
                    	<li><a href="../inicial.php">Home</a></li>
                        <li><a href="index.php">Listar</a></li>
                    </ul>
            	</div>	                	
		 </div>
         
         
         
         
         
         <!-- Div Miolo -->         
         <div id="miolo">
         	
            <!-- Navegação -->
            <div id="navegacao">
            	<a href="../inicial.php">Home</a><a href="index.php"></a> 
            </div>
            
            <!-- Cabeçalho -->
            <div class="cabecalho_miolo">
            	 Alteração
            </div>
            
            <!-- div erros -->
            <div id="erro" style=" <?php echo ($ok ? 'display:none;':'display:block;')   ?> "><?php echo ($ok ? '':"$html")  ?>
            </div>
            
            
            
            
            
            
            
            <!---------------------------------------------------------------------------------------------------------------->
            <!-- Dentro Miolo -->
            <!---------------------------------------------------------------------------------------------------------------->
            <div id="dentro_miolo">
				<?php
					//	CRIO OS JAVASCRIPT PARA OS CAMPOS OBRIGATORIOS
					Util::campo_obrigatorio_js($obj_empresas->campos());
				?>
                
                <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" enctype="multipart/form-data" onSubmit='return valida_campos_formulario()'>
				  
               
				  
                  
				  
                  <div class="nome_campos">Descrição:</div>
                  <?php $obj_jquery->ckeditor('descricao', $row[descricao]); ?>
                  
                 
                  
                  
                  
		          <div id="quebra"></div>
                  <input type="hidden" name="titulo"  id="titulo" value="<?php echo $row[titulo]; ?>" />
                   
                   
                   
                   
                   
                   
       
                   
                   
                   
                   
                   <!----------------------------------------------------------------------->
                   <!--	ACOES DO FORMULARIO	-->
                   <!----------------------------------------------------------------------->
                   <div id="btn_submit">
                   		<input name="id" type="hidden" value="<?php echo $_GET[id]; ?>""/>
                        <input name="btn_cadastrar" type="submit" value="Alterar" class="class_btn_submit"/>
                   </div>
                  
                
                </form>
                
                <!-- btn voltar -->
                <div id="btn_voltar">
                	<a href="../inicial.php"><img src="../img/voltar.png" border="0" /></a>
                </div>
                
			</div>
         
         </div>	
	</div>
</div>
</body>

</html>
