<?php
require_once("../../class/Cabecalho.class.php");
require_once("Tv.class.php");
$obj = new Tv();
$caminho_projeto = Util::caminho_projeto();



//	VERIFICO SE PARA EFETUAR O CADASTRO
if(isset($_POST[btn_cadastrar]))
{
	$obj->cadastrar();
}

$row = $_POST;




?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="language" content="pt-br" /> 
<link href="../estilo/estilo.css" rel="stylesheet" type="text/css" />



<!------------------------------------------------------------------------------------------------------------------->
<!--	JSCRIPT	-->
<!------------------------------------------------------------------------------------------------------------------->
<?php
	$obj_jquery = new Biblioteca_Jquery();	
	
	$obj_jquery->biblioteca_masked();
	
	//	CRIO AS MASCARAS
	$obj_jquery->mascara_campos($obj->campos());	
	
	//	CRIO OS JAVASCRIPT PARA OS CAMPOS OBRIGATORIOS
	Util::campo_obrigatorio_js($obj->campos());
	
	
?>





<title>Administração do site</title>
</head>

<body>

	<!-- Começa -->
<div id="geral">
	<!-- Div para topo -->
	<div id="topo"></div>
    
	<!-- Div para linha no bg -->
	<div id="linhaMenu">
		<div id="cabecalho">Gerenciando </div>
		<div id="sair"><a href="../logout.php"><img src="../img/sair.png" border="0" /></a> </div>
	</div>
    
	<!-- Div conteúdo -->
	<div id="conteudo">

	  
      <!---------------------------------------------------------------------------------------------------------------->
      <!-- Div menu lateral -->
      <!---------------------------------------------------------------------------------------------------------------->
		<div id="menu_left">
        	<div class="cabecalho_menu_left"></div>
            	<div id="menu_left_dentro">
                    <ul>
                        <li><a href="../inicial.php">Home</a></li>
                        <li><a href="index.php">Listar</a></li>
                        <li><a href="cadastro.php">Cadastrar</a></li>    
                    </ul>
               </div>	                	
		 </div>
         
         
         
          <!-- Div Miolo -->         
         <div id="miolo">
         	
            <!-- Navegação -->
            <div id="navegacao">
            	<a href="../inicial.php">Home</a><a href="index.php"></a> 
            </div>
            
            <!-- Cabeçalho -->
            <div class="cabecalho_miolo">
            	 Cadastro
            </div>
            
            
            
            
            <!---------------------------------------------------------------------------------------------------------------->
            <!-- Dentro Miolo -->
            <!---------------------------------------------------------------------------------------------------------------->
            <div id="dentro_miolo">
            
            	
                
                
            
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data" onSubmit='return valida_campos_formulario()'>
                    
                 
                    
                   
				   <?php $obj->campos_formulario($row); ?>
                    
                    
                  
                  
                   
                   
                   <div id="quebra"></div>
                   <!----------------------------------------------------------------------->
                   <!--	ACOES DO FORMULARIO	-->
                   <!----------------------------------------------------------------------->
                   <input name="btn_cadastrar" type="submit" value="Cadastrar" class="class_btn_submit"/>
                   
                  
                
                </form>
            <!-- btn voltar -->
                <div id="btn_voltar">
                	<a href="../inicial.php"><img src="../img/voltar.png" border="0" /></a>
                </div>
                
			</div>
         
         </div>	
	</div>
</div>
</body>

</html>
