<?php
require_once("../../class/Include.class.php");




class Email extends Dao
{
	

		public $nome_tabela = "tb_contatos";
		public $id_tabela = "idcontato";
	
	
	
	
	
		#-------------------------------------------------------------------------------------------------#
		#	CAMPOS DO FORMULARIO
		#-------------------------------------------------------------------------------------------------#
		public function campos()
		{
			$campos = array(
							array(
								  'nome_campo_form'		=>	'nome',				//	NOME DO CAMPO NO FORMULARIO			
								  'obr'					=>	's',					//	INFORME SE O CAMPO � OBRIGATORIO
								  'msgerros'			=>	'Informe o nome da categoria',		//	MENSAGEM DE OBRIGATORIEDADE
								  'tipo'				=>	'texto',				//	TIPO DE DADOS DO CAMPO (texto, moeda, data, telefone, cep)
								  'alinhamento'			=>	'left',					//	ALINHAMENTO DO TEXTO
								  'link'				=>	's',					//	CRIA UM LINK PARA ORDENACAO
								  'nome_coluna'			=>	'NOME',				//	NOME QUE SER� EXIBIDO NA COLUNA
								  'exibir_listagem'		=>	's'						//	INFORME SE DEVERA SER EXIBIDO NA LISTAGEM
								  )
							);
			return $campos;
		}
	
		
		
		#-------------------------------------------------------------------------------------------------#
		#	CADASTRA OS DADOS
		#-------------------------------------------------------------------------------------------------#
		public function cadastrar()
		{				
			//	VERIFICO SE NAO HOUVE ERRO NO FORMULARIO
			if($this->get_dados_formulario() == false)
			{					
				//	CADASTRO OS DADOS
				$this->set_cadastro_dados();
				
				//	BUSCO O ID
				$id = parent::get_ultimo_id($this->id_tabela, $this->nome_tabela);
				
				
				//	VERIFICO SE EXISTE IMAGEM
				if($_FILES[imagem_banner][name] != "")
				{
					//	EFETUO O UPLOAD DA IMAGEM
					$nome_arquivo = Util::upload_imagem("../../uploads", $_FILES[imagem_banner], "3145728");	
					
					//	TRATO A IMAGEM
					//$this->trata_imagem("../../uploads", $nome_arquivo);				
					
					//	ATUALIZA NA TABELA O NOME DA IMAGEM
					$this->atualiza_nome_imagem_tabela($id, "imagem_banner", "$nome_arquivo");
				}
				
				
				//	MATA AS VARIAVEIS DE SESSAO
				$this->mata_session();
	
				//	MSG DE SUCESSO
				Util::script_msg("Cadastro efetuado com sucesso");
	
				// 	REDIRECIONA PARA A PAGINA
				Util::script_location("cadastro.php");
			}
		}
		
		
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	ALTERA OS DADOS
		#-------------------------------------------------------------------------------------------------#
		public function alterar($id)
		{	
			//	VERIFICO SE NAO HOUVE ERRO NO FORMULARIO
			if($this->get_dados_formulario() == false)
			{					
				//	CADASTRO OS DADOS
				$this->set_altera_dados($id);
				
				
				//	VERIFICO SE EXISTE IMAGEM
				if($_FILES[imagem_banner][name] != "")
				{
					//	EFETUO O UPLOAD DA IMAGEM
					$nome_arquivo = Util::upload_imagem("../../uploads", $_FILES[imagem_banner], "3145728");	
					
					//	TRATO A IMAGEM
					//$this->trata_imagem("../../uploads", $nome_arquivo);				
					
					//	ATUALIZA NA TABELA O NOME DA IMAGEM
					$this->atualiza_nome_imagem_tabela($id, "imagem_banner", "$nome_arquivo");
				}
				
				
				//	MATA AS VARIAVEIS DE SESSAO
				$this->mata_session();
	
				//	MSG DE SUCESSO
				Util::script_msg("Alteracao efetuada com sucesso");
	
				// 	REDIRECIONA PARA A PAGINA
				Util::script_location("index.php");
			}
			else
			{
				//	RETORNO OS DADOS DO FORMUL�RIO
				$obj_formulario = new Formulario($this->campos());
				return $obj_formulario->get_dados_formulario();
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	ALTERO OS DADOS DA TABELA
		#-------------------------------------------------------------------------------------------------#
		private function atualiza_nome_imagem_tabela($id, $coluna, $nome_imagem)
		{			
			//	ATUALIZO OS DADOS NA TABELA
			$sql = "UPDATE ". $this->nome_tabela ." SET $coluna = '$nome_imagem' WHERE ". $this->id_tabela ." = '$id' ";
			parent::executaSQL($sql);
		}
		
		
		
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	TRATO A IMAGEM
		#-------------------------------------------------------------------------------------------------#
		public function trata_imagem($caminho, $arquivo)
		{
			//	CARREGO A IMAGEM 
			$image = new Imagem();
			$image->load("$caminho/$arquivo");
			//	CRIO A IMAGEM
			$image->redimension_pela_proporcao(600);
			$image->save("$caminho/$arquivo");
			//	CRIO O TUMB
			$image->redimension_pela_proporcao(250);
			$image->save("$caminho/tumb_$arquivo");
		}
		
		
		
		
		
		
		
		
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	BUSCO OS DADOS DO FORMULARIO
		#-------------------------------------------------------------------------------------------------#
		private function get_dados_formulario()
		{
	
			//	BUSCO OS DADOS DO FORMUL�RIO
			$obj_formulario = new Formulario($this->campos());
	
			//	VERIFICO SE RETORNOU ALGUM ERRO NO OBJETO, SEN�O CADASTRO OS DADOS NO BANCO
			if ($obj_formulario->get_erros() != 1)
			{
				$obj_formulario->get_msg_erros();
				return true;
			}
			else
			{
				// PEGO OS CAMPOS
				$_SESSION[campos] = $obj_formulario->get_campos();
	
				// PEGO OS DADOS
				$_SESSION[dados] = $obj_formulario->get_dados_formulario();
			}
		}
		
		
		
		
		
		
		
		
	
		
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	CADASTRO NA TABELA
		#-------------------------------------------------------------------------------------------------#
		private function set_cadastro_dados()
		{
			if (parent::executaINSERT($this->nome_tabela, $_SESSION[campos], $_SESSION[dados]) != 0)
			{
				$this->get_msg_erro();
			}
		}
	


		#-------------------------------------------------------------------------------------------------#
		#	ALTERO OS DADOS DA TABELA
		#-------------------------------------------------------------------------------------------------#
		private function set_altera_dados($id)
		{			
			//	ATUALIZO OS DADOS NA TABELA
			if (parent::executaALTERACAO($this->nome_tabela, $_SESSION[campos], $_SESSION[dados], $this->id_tabela, $id) != 0)
			{
				$this->get_msg_erro();
			}
		}
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	MATO AS VARIAVEIS DE SESS�O DA CLASSE
		#-------------------------------------------------------------------------------------------------#
		private function mata_session()
		{
			unset($_SESSION['campos']);
			unset($_SESSION['dados']);
		}
		
		
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	BUSCO OS DADOS DA TABELA
		#-------------------------------------------------------------------------------------------------#
		public function get_dados_tabela($id)
		{					
			//	BUSCO OS DADOS
			$sql = "SELECT * FROM ". $this->nome_tabela ." WHERE ". $this->id_tabela ." = '$id' ";
			$result = parent::executaSQL($sql);
			return mysql_fetch_array($result);
		}
		
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	EXCLUSAO
		#-------------------------------------------------------------------------------------------------#
		public function excluir($id)
		{
			//if($this->verifica_relacionamento($id) == 0)
			//{
				$sql = "DELETE FROM ". $this->nome_tabela ." WHERE ". $this->id_tabela ."= '$id'";
				parent::executaSQL($sql);
				Util::script_msg("Item exclu�do com sucesso.");
			//}
			//else
			//{
			//	Util::script_msg("A categoria n�o pode ser exclu�da, porque possui produtos nela, exclua primeiramente os produtos");
			//	Util::script_go_back();
			//	exit();
			//}
		}
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	EXCLUSAO
		#-------------------------------------------------------------------------------------------------#
		public function verifica_relacionamento($id)
		{
			$sql = "SELECT * FROM tb_produtos WHERE id_categoria = '$id'";
			return mysql_num_rows(parent::executaSQL($sql));
		}
		
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	DESATIVA O PRODUTO
		#-------------------------------------------------------------------------------------------------#
		public function ativar_desativar($id, $acao)
		{			
			//	ATUALIZO OS DADOS NA TABELA
			$sql = "UPDATE ". $this->nome_tabela ." SET ATIVO = '$acao' WHERE ". $this->id_tabela ." = '$id' ";
			parent::executaSQL($sql);
			
			
			switch($acao)
			{
				case "SIM":
					Util::script_msg("Item ativado com sucesso.");
				break;
				case "NAO":
					Util::script_msg("Item desativado com sucesso.");
				break;
			}
			
		}
		
		
		
		
		#-------------------------------------------------------------------------------------------------#
		#	DESATIVA O PRODUTO
		#-------------------------------------------------------------------------------------------------#
		public function atualiza_ordem_registro($id, $ordem)
		{
			$sql = "UPDATE ". $this->nome_tabela ." SET ORDEM = '$ordem' WHERE ". $this->id_tabela ." = '$id' ";
			parent::executaSQL($sql);
		}
		
		
		
	
	
	
}




















?>












