<?php
require_once("../../class/Cabecalho.class.php");
require_once("Email.class.php");
$obj_site = new Site();
$obj = new Email();
$caminho_projeto = Util::caminho_projeto();



//	VERIFICO SE PARA EFETUAR O CADASTRO
if(isset($_POST[btn_cadastrar]))
{
	$obj->alterar(base64_decode($_POST[id]));
}
else
{
	$idcontato = base64_decode($_GET[id]);
	$row = $obj->get_dados_tabela(base64_decode($_GET[id]));
	
	$sql = "UPDATE tb_contatos SET lido = 'SIM' WHERE idcontato = '$idcontato'";
	$obj->executaSQL($sql);
}




?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="language" content="pt-br" /> 
<link href="../estilo/estilo.css" rel="stylesheet" type="text/css" />



<!------------------------------------------------------------------------------------------------------------------->
<!--	JSCRIPT	-->
<!------------------------------------------------------------------------------------------------------------------->
<?php
$obj_jquery = new Biblioteca_Jquery();	

$obj_jquery->biblioteca_masked();

//	CRIO AS MASCARAS
$obj_jquery->mascara_campos($obj->campos());	

//	CRIO OS JAVASCRIPT PARA OS CAMPOS OBRIGATORIOS
Util::campo_obrigatorio_js($obj->campos());
?>






<title>Administração do site</title>
</head>

<body>




	<!-- Começa -->
<div id="geral">
	<!-- Div para topo -->
	<div id="topo"></div>
    
	<!-- Div para linha no bg -->
	<div id="linhaMenu">
		<div id="cabecalho">Gerenciando </div>
		<div id="sair"><a href="../logout.php"><img src="../img/sair.png" border="0" /></a> </div>
	</div>
    
	<!-- Div conteúdo -->
	<div id="conteudo">

	  
      <!---------------------------------------------------------------------------------------------------------------->
      <!-- Div menu lateral -->
      <!---------------------------------------------------------------------------------------------------------------->
		<div id="menu_left">
        	<div class="cabecalho_menu_left"></div>
            	<div id="menu_left_dentro">
                    <ul>
                        <li><a href="../inicial.php">Home</a></li>
                        <li><a href="index.php">Listar</a></li>
                    </ul>
				</div>	                	
		 </div>
         
         
         
          <!-- Div Miolo -->         
         <div id="miolo">
         	
            <!-- Navegação -->
            <div id="navegacao">
            	<a href="../inicial.php">Home</a><a href="index.php"></a> 
            </div>
            
            <!-- Cabeçalho -->
            <div class="cabecalho_miolo">
            	 Cadastro
            </div>
            
            
            
            
            <!---------------------------------------------------------------------------------------------------------------->
            <!-- Dentro Miolo -->
            <!---------------------------------------------------------------------------------------------------------------->
            <div id="dentro_miolo">
                
                <?php
				if(isset($_POST[id_login]))
				{
					$de = utf8_decode("Gerência");
					$data = date("Ymd");
					$hora = date("H:i:s");
					$assunto = utf8_decode("Mensagem encaminhada pela gerência");
					$para = $_POST[id_login];
					$mensagem = "
											<br />
											<p>Dados do cliente</p>
								
											Nome: $row[nome]
											<br />
											
											Email: $row[email]
											<br />
											
											Telefone: $row[telefone]
											<br />
											
											Celular: $row[celular]
											<br /><br />
											
											
											Mensagem:<br />
											$row[mensagem]
											<br /><br />
											
											";
					
					//	CADASTRA O USUARIO
					$sql = "
							INSERT INTO tb_mensagens
							(assunto, mensagem, data, hora, de, para)
							VALUE
							('$assunto', '$mensagem', '$data', '$hora', '$de', '$para')
							";
					$obj_site->executaSQL($sql);
					
					
					
					//	ATUALIZO NA TABELA PARA QUEM FOI ENCAMINHADO
					$sql = "UPDATE	tb_contatos SET id_login_encaminhada = '$para' WHERE idcontato = '$idcontato'";
					$obj_site->executaSQL($sql);
					
					Util::script_msg("Mensagem encaminhada com sucesso.");
					Util::script_location("./");
				}
				?>
                
                
                <form action="" method="post" enctype="multipart/form-data" onSubmit='return valida_campos_formulario()'>
                   
                   	
                    <div class="div_campos_left">
                        <div class="nome_campos">Nome:</div>
                        <?php Util::imprime($row[nome]); ?>
                    </div>
                    
                    
                    <div class="div_campos_right">
                        <div class="nome_campos">Email:</div>
                        <?php Util::imprime($row[email]); ?>
                    </div>
                    
                    
                    
                    
                    <div id="quebra"></div>
                    
                    
                    <div class="div_campos_left">
                        <div class="nome_campos">Telefone:</div>
                        <?php Util::imprime($row[telefone]); ?>
                    </div>
                    
                    <div class="div_campos_right">
                        <div class="nome_campos">Celular:</div>
                        <?php Util::imprime($row[celular]); ?>
                    </div>
                    
                    
                    <div id="quebra"></div>
                    
                    
                    
                    
                    
                    <div class="div_campos_left">
                    	<div class="nome_campos">Data:</div>
                        <?php echo Util::formata_data($row[data]); ?>
                    </div>
                    
                    
                    <div class="div_campos_right">
                    	<div class="nome_campos">Hora:</div>
                        <?php Util::imprime($row[hora]); ?>
                    </div>

                    <div id="quebra"></div>
                    <div class="div_campos_left">
                        <div class="nome_campos">Assunto:</div>
                        <?php Util::imprime($row[assunto]); ?>
                    </div>
                    
                    <div class="div_campos_right">
                         <div class="nome_campos">Receber Informativo:</div>
                        <?php Util::imprime($row[receber_noticias]); ?>
                    </div>
                    
                    <br /><br />
                    <div id="quebra"></div>
                    
                    <div class="nome_campos">Mensagem:</div>
                    <?php Util::imprime($row[mensagem]); ?>
                   
                   
                   
                   
                   
                   
                   	<?php /*?>	<?php
						if($row[id_login_encaminhada] == 0)
						{
                        ?>
                            <div style="background:#e5e5e5; margin:30px 0px 0px 0px; padding:20px;">
                               <fieldset>
                                    <legend>Encaminhar mensagem para:</legend>
                                    <p><?php Util::cria_select_bd("tb_logins", "idlogin", "nome", "id_login", "", "", "Selecione o corretor") ?></p>
                                    <input type="image" src="../img/btn-enviar.jpg" alt="Enviar" />
                               </fieldset>
                           </div>
                        <?php
						}
						?><?php */?>
                   
                   
                   
                   
                   
                   
                   
                   <div id="quebra"></div>
                   <!----------------------------------------------------------------------->
                   <!--	ACOES DO FORMULARIO	-->
                   <!----------------------------------------------------------------------->
                    <input name="id" type="hidden" value="<?php echo $_GET[id]; ?>"/>
                    <!--<input name="btn_cadastrar" type="submit" value="Alterar" class="class_btn_submit"/> -->
                  
                  
                  	<br /><br /><br />
                    
                
                </form>
                <!-- btn voltar -->
                <div id="btn_voltar">
                	<a href="../inicial.php"><img src="../img/voltar.png" border="0" /></a>
                </div>
                
			</div>
         
         </div>	
	</div>
</div>



</body>

</html>
