<?php
class trataimg
{
	public $urlAbre;
	public $urlSalva;
	public $nomeImgAbre;
	public $nomeImgSalva;
	public $logo;
	public $tamImgGrande;
	public $tamImgTum; 
	public $bollTum;
	public $bollLogo;
	public $tamImgGrandeW;
	public $tamImgGrandeH;
	public $tamImgTumW;
	public $tamImgTumH;	
	
	public function setUrlAbre($uAbre){
		$this->urlAbre = $uAbre;
	}
	public function setUrlSalva($uSalva){
		$this->urlSalva = $uSalva;
	}
	public function setNomeImgAbre($iAbre){
		$this->nomeImgAbre = $iAbre;
	}
	public function setNomeImgSalva($iSalva){
		$this->nomeImgSalva = $iSalva;
	}
	public function setLogo($slogo){
		$this->logo = $slogo;
	}
	public function setTamImgGrande($tGrande){
		$this->tamImgGrande = $tGrande;
	}
	public function setTamImgTum($tTum){
		$this->tamImgTum = $tTum;
	}
	public function setBollTum($bTum){
		$this->bollTum = $bTum;
	}
	public function setBollLogo($bLogo){
		$this->bollLogo = $bLogo;
	}
	public function setBollTum2($bTum2){
		$this->bollTum2 = $bTum2;
	}
	public function setTamImgTum2($tTum2){
		$this->tamImgTum2 = $tTum2;
	}
	public function setTamImgGrandeW($wTam){
		$this->tamImgGrandeW = $wTam;
	}
	public function setTamImgGrandeH($hTam){
		$this->tamImgGrandeH = $hTam;
	}
	public function setTamImgTumW($wTam){
		$this->tamImgTumW = $wTam;
	}
	public function setTamImgTumH($hTam){
		$this->tamImgTumH = $hTam;
	}	
	
		
	public function trataFoto()
	{
		chmod($this->urlAbre.$this->imgAbre,0777);
		$img_velha = imagecreatefromjpeg($this->urlAbre.$this->nomeImgAbre);			
		$largurao = imagesx($img_velha);
		$alturao = imagesy($img_velha);
		if (!empty($this->tamImgGrande))
		{
				if ($largurao > $alturao)
					$cal = $largurao;
				else
					$cal = $alturao;
					
				$prop = (100*$this->tamImgGrande/$cal)/100;
				$alturad = $alturao * $prop; 			
				$largurad = $largurao * $prop; 	
		}
		else
		{
			$alturad = $this->tamImgGrandeH; 			
			$largurad = $this->tamImgGrandeW;
		}	
		
		$img_nova = imagecreatetruecolor( $largurad , $alturad );
		imagecopyresampled($img_nova,$img_velha, 0, 0, 0, 0, $largurad,$alturad, $largurao, $alturao);
		if ($this->bollLogo)
		{
			$logo = imagecreatefromjpeg( $this->logo );
			imagecopymerge($img_nova , $logo , $largurad - (imagesx($logo) + 5) , $alturad - ( imagesy($logo) + 5 ) , 0 , 0 , imagesx($logo) , imagesy($logo) , 70);
		}	
		
		imagejpeg($img_nova, $this->urlSalva.$this->nomeImgSalva);		
		imagedestroy($img_nova);
		imagedestroy($img_velha);
		if ($this->bollLogo)
			imagedestroy($logo);
			
		//tumb///
		if ($this->bollTum)
		{
			$im = imagecreatefromjpeg($this->urlSalva.$this->nomeImgSalva);
			$largurao = imagesx($im);
			$alturao = imagesy($im);
			if (!empty($this->tamImgTum))
			{
				if ($largurao > $alturao)
					$cal = $largurao;
				else
					$cal = $alturao;
				$prop = (100*$this->tamImgTum/$cal)/100;
				$alturad = $alturao * $prop; 			
				$largurad = $largurao * $prop; 	
			}
			else
			{
				$alturad = $this->tamImgTumH; 			
				$largurad = $this->tamImgTumW;
			}	
			$nova = imagecreatetruecolor($largurad,$alturad);
			imagecopyresampled($nova,$im,0,0,0,0,$largurad,$alturad,$largurao,$alturao);
			imagejpeg($nova,$this->urlSalva."t".$this->nomeImgSalva);
			imagedestroy($nova);
			imagedestroy($im);
		}
		if ($this->bollTum2)
		{
			$im = imagecreatefromjpeg($this->urlSalva.$this->nomeImgSalva);
			$largurao = imagesx($im);
			$alturao = imagesy($im);
			
			if ($largurao > $alturao)
				$cal = $largurao;
			else
				$cal = $alturao;
			$prop = (100*$this->tamImgTum2/$cal)/100;
			$alturad = $alturao * $prop; 			
			$largurad = $largurao * $prop; 	
			
			$nova = imagecreatetruecolor($largurad,$alturad);
			imagecopyresampled($nova,$im,0,0,0,0,$largurad,$alturad,$largurao,$alturao);
			imagejpeg($nova,$this->urlSalva."s".$this->nomeImgSalva);
			imagedestroy($nova);
			imagedestroy($im);
		}
		
		
		
	}	
	public function tumb2()
	{
		$targ_w =$this->tamImgTumW;
		$src = $this->urlAbre.$this->nomeImgAbre;
		$img_r = imagecreatefromjpeg($src);
		list($width_orig, $height_orig) = getimagesize($src);		
		$prop = (100*$targ_w/$width_orig)/100;		
		$w = $width_orig*$prop;
		$h = $height_orig*$prop;		
		$h1 = ($height_orig*25)/100;
		$dst_r = ImageCreateTrueColor( $w, $this->tamImgTumH );
		imagecopyresampled($dst_r,$img_r,0,0,0,$h1,$w,$h,$width_orig,$height_orig);		
		imagejpeg($dst_r,$this->urlSalva."r".$this->nomeImgSalva);
		imagedestroy($img_r);
		imagedestroy($dst_r);
		
	}
}


?>