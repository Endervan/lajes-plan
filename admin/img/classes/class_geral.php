<?php
require_once("class_bd.php");
class geral extends bd
{
	protected $_nomeTabela;
	protected $_nomeChave;
		
	public function carregaDados(array $arDados)
	{
		$arAtributos = get_object_vars( $this );
		foreach ($arAtributos as $arCampos => $valor)
			if ($arCampos['0'] != "_")
				if ($arCampos == "senha")
					$this->$arCampos = md5($arDados["$arCampos"]);
				else if ($arCampos == "data")
					$this->$arCampos = $this->formataData($arDados["$arCampos"],"banco");	
				else if ($arCampos == "nome" || $arCampos == "titulo" )
					$this->$arCampos = $this->tiraChar(utf8_decode(addslashes(trim($arDados["$arCampos"]))));
				else
					$this->$arCampos = utf8_decode(addslashes(trim($arDados["$arCampos"])));
	} 
	
	public function carregaDadosBd(array $arDados)
	{
		$arAtributos = get_object_vars( $this );
		foreach ($arAtributos as $arCampos => $valor)
			if ($arCampos['0'] != "_")
				$this->$arCampos = $arDados["$arCampos"];			
	}
	
	public function insert()
	{
		$strCampos = "";
		$strValor = "";
		$arAtributos = get_object_vars( $this );
		foreach ($arAtributos as $arCampos => $valor)
			if ($arCampos['0'] != "_")
			{
				$strCampos .= $arCampos.", ";
				$strValor .= "'".$this->$arCampos."', ";
			}	
		$strCampos = substr($strCampos,0,-2);
		$strValor = substr($strValor,0,-2);
		$dbQuery = "INSERT INTO ".$this->_nomeTabela."( ".$strCampos." )";
		$dbQuery .= " VALUES ( ".$strValor." )";
		return $this->exeQuery($dbQuery);
		
	}
	
	public function select($id = null,$query = null,$order=null,$limit=null)
	{
		$dbQuery = "SELECT * FROM ".$this->_nomeTabela." ";
		$dbQuery .= " WHERE status = 1";
		if (!empty($id))
			$dbQuery .= " AND ".$this->_nomeChave." = $id";
		if (!empty($query))
			$dbQuery .= $query;	
		if (!empty($order))	
			$dbQuery .= $order;
		else	
			$dbQuery .= " ORDER BY ".$this->_nomeChave." DESC ";	
		if (!empty($limit))	
			$dbQuery .= $limit;
			
		$dbResult = $this->exeQuery($dbQuery);
		$linha = mysql_num_rows($dbResult);
		if ($linha == 0)
			return $linha;
		else
		{
			if (!empty($id))
			{
				$arDados = mysql_fetch_array($dbResult);
				return $arDados;
			}
			else
			{
				$arRetorno = array();
				while ($arDados = mysql_fetch_array($dbResult))
				{
					$arRetorno[] = $arDados;
				}
				return $arRetorno;
			}
		}	
	}
	
	public function conta($id = null,$query = null,$order=null,$limit=null)
	{
		$dbQuery = "SELECT * FROM ".$this->_nomeTabela." ";
		$dbQuery .= " WHERE status = 1";
		if (!empty($id))
			$dbQuery .= " AND ".$this->_nomeChave." = $id";
		if (!empty($query))
			$dbQuery .= " ".$query;	
		if (!empty($order))	
			$dbQuery .= " ".$order;
		else	
			$dbQuery .= " ORDER BY ".$this->_nomeChave." DESC ";	
		if (!empty($limit))	
			$dbQuery .= $limit;
			
		$dbResult = $this->exeQuery($dbQuery);
		$linha = mysql_num_rows($dbResult);
		return $linha;
	}	
	
	public function del($id)
	{
		$dbQuery = "DELETE FROM ".$this->_nomeTabela;
		$dbQuery .= " WHERE ".$this->_nomeChave." = $id";
		$this->exeQuery($dbQuery);
	}
	
	public function desativa($id)
	{
		$dbQuery = "UPDATE ".$this->_nomeTabela." SET status = 0";
		$dbQuery .= " WHERE ".$this->_nomeChave." = $id";
		$this->exeQuery($dbQuery);
	}
	
	public function update($id)
	{
		$strSet = "";
		$arAtributos = get_object_vars( $this );
		foreach ($arAtributos as $arCampos => $valor)
			if ($arCampos['0'] != "_" && !empty($this->$arCampos))
				$strSet .= $arCampos." = '".$this->$arCampos."', ";
		
		$strSet = substr($strSet,0,-2);
		$dbQuery = "UPDATE ".$this->_nomeTabela." SET $strSet";
		$dbQuery .= " WHERE ".$this->_nomeChave." = $id";
		$this->exeQuery($dbQuery);
	}
	
	public function carregaDadosPorId($id)
	{
		$arDados = $this->select($id);
		if ($arDados != 0)
			$this->carregaDadosBd($arDados);	
	}
	
	public function verifica($campo,$valor,$campo2 = null,$valor2 = null,$id = null)
	{
		$dbQuery = "SELECT * FROM ".$this->_nomeTabela;
		$dbQuery .= " WHERE status = 1";		
		$dbQuery .= " AND $campo = '$valor'";
		if (!empty($campo2))
			$dbQuery .= " AND $campo2 = '$valor2'";
		if (!empty($id))
			$dbQuery .= " AND ".$this->_nomeChave." != $id";
		$dbResult = $this->exeQuery($dbQuery);
		$linha = mysql_num_rows($dbResult);
		return $linha;	
	}
	public function formataData($data,$tipo)
	{
		$dataf = "";
		if ($tipo == "banco")
		{
			if ($data[2] == "/")
			{
				$dia = substr($data,0,2);
				$mes = substr($data,3,2);
				$ano = substr($data,6,4);
				$dataf = $ano."-".$mes."-".$dia;
			}
			else
				$dataf = $data;			
		}
		if ($tipo == "exibir")
		{
			if ($data[4] == "-")
			{
				$dia = substr($data,8,2);
				$mes = substr($data,5,2);
				$ano = substr($data,0,4);
				$dataf = $dia."/".$mes."/".$ano;
			}
			else
				$dataf = $data;			
		}
		return $dataf;	
	}
	public function formataData2($data)
	{
		$arMes = array ('Janeiro','Fevereiro','Mar�o','Abril','Maio ','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');
		$dia = substr($data,8,2);
		$mes = substr($data,5,2);
		$ano = substr($data,0,4);
		$diaS = $this->diasemana($data);
		$dataf = $diaS.", ".$dia." de ".$arMes[$mes - 1]." de ".$ano;
		return $dataf;	
	}
	public function diasemana($data) 
	{
		$dia = substr($data,8,2);
		$mes = substr($data,5,2);
		$ano = substr($data,0,4);
	
		$diasemana = date("w", mktime(0,0,0,$mes,$dia,$ano) );
	
		switch($diasemana) 
		{
			case"0": $diasemana = "Domingo";       break;
			case"1": $diasemana = "Segunda-Feira"; break;
			case"2": $diasemana = "Ter�a-Feira";   break;
			case"3": $diasemana = "Quarta-Feira";  break;
			case"4": $diasemana = "Quinta-Feira";  break;
			case"5": $diasemana = "Sexta-Feira";   break;
			case"6": $diasemana = "S�bado";        break;
		}
		
		return $diasemana;
		
	}


	public function verificaTipo($arquivo)
	{
		$tamStr = strlen($arquivo)-4;
		$tipo = substr($arquivo,$tamStr,4);
		$tipo = strtolower($tipo);
		$tipo = str_replace(".","",$tipo);
		return $tipo;	
	}
	public function enviaArquivo($arquivo,$arTipo,$local_temp,$local_salva)
	{
		$tipoArquivo = $this->verificaTipo($arquivo);
		if (empty($tipoArquivo))
			return "Nenuhum arquivo selecionado";
		else	
		{
			$comparaTipo = false;
			foreach($arTipo as $ctipo)
				if ($tipoArquivo == $ctipo)
					$comparaTipo = true;
			
			if ($comparaTipo)
			{				
				$mover = move_uploaded_file($local_temp,$local_salva);
				if (!$mover)
					return "N�o foi possivel enviar o arquivo";
				else
					return "ok";	
			}
			else
				return "Tipo de Arquivo n�o permitido";
		}		
	
	
	}
	public function formataTexto($texto,$limite)
	{
		$textof = substr($texto,0,$limite);
		if ( strlen($texto) > $limite)
			$textof = $textof." ...";
		return utf8_encode(strip_tags($textof));	
	}
	public function formataNome($nome)
	{
		
		$nomef = substr($nome,0,strpos($nome," "));
		return utf8_encode($nomef);	
	}
	public function formataLink($link)
	{
		$link = $this->formataAcento($link);
		$link = strtolower(str_replace("/","|",$link));
		$link = strtolower(str_replace(":","",$link));
		$link = strtolower(str_replace("-",".",$link));
		$link = strtolower(str_replace(" ","-",$link));
		return  utf8_encode($link);
	
	}
	public function formataAcento( $texto ) 
	{ 
	  $array1 = array(   "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�" 
						 , "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�" ); 
	  $array2 = array(   "a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c" 
						 , "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C" ); 
	  return  str_replace( $array1, $array2, $texto); 
	} 
	public function imprime($texto)
	{
		$texto = str_replace("<DIV","<p",$texto);
		$texto = str_replace("</DIV>","</p>",$texto);
		$texto = str_replace("<div","<p",$texto);
		$texto = str_replace("</div>","</p>",$texto);
		$texto = utf8_encode(stripslashes($texto));
		return $texto;	
	}
	public function tiraChar($char)
	{
		$char = str_replace("/"," ",$char);
		$char = str_replace("-","",$char);	
		$char = str_replace("  "," ",$char);
		$char = str_replace(":","",$char);
		$char = str_replace("'"," ",$char);
		return $char;
	}
	public function verificaEmail($email)
	{
		if (!eregi("^[a-z0-9_\.\-]+@[a-z0-9_\.\-]*[a-z0-9_\-]+\.[a-z]{2,4}$", $email))
		{
			return 0;
		}
		else
		{
			return 1;	
		}
	
	}
	public function videoY($link)
	{
		$link = str_replace("?v=","/v/",$link);
		return $link;	
	}
	public function verificaLink($link)
	{
		$verifica = substr($link,0,7);
		if ($verifica == "http://")
			return $link;
		else
			return "http://".$link;		
	}


} 

?>