<?php
require_once("class_geral.php");
class img_produto extends geral
{
	public $_nomeTabela = 'img_produto';
	public $_nomeChave = 'img_produto_id';
	
	public $_urlAbre = 'foto_produto/original/';	
	public $_urlSalva = 'foto_produto/';	
	public $_logo = 'marcadagua.jpg';	
	public $_tamImgGrande = 640;	
	public $_tamImgTum = 120;	
	public $_bollTum = true;	
	public $_bollLogo = false;	
	public $_bollTum2 = false;	
		
	public $produto_id;
	public $foto;
	public $legenda;
	public $foto_tratada;
	
	public function updateImgTratada($id,$nome_img)
	{
		$dbQuery = "UPDATE ".$this->_nomeTabela." SET";
		$dbQuery .= " tratada = 1,foto_tratada = '$nome_img'";
		$dbQuery .= " WHERE ".$this->_nomeChave." = $id";
		$this->exeQuery($dbQuery);
	
	}
}

?>
