<?php
require_once("class_geral.php");
class produto extends geral
{
	public $_nomeTabela = 'produto';
	public $_nomeChave = 'produto_id';
	
	public $_urlAbre = 'foto_produto_capa/original/';	
	public $_urlSalva = 'foto_produto_capa/';	
	public $_logo = 'marcadagua.jpg';	
	public $_tamImgGrande = 320;	
	public $_tamImgTumW = 203;	
	public $_tamImgTumH = 88;	
	public $_bollTum = false;	
	public $_bollLogo = false;	
		
	public $nome;
	public $categoria_id;
	public $cod;
	public $descricao;
	public $produtos;
	public $valores;
	public $foto;
	
	public $_nomePag = " Produtos";
	
	

}

?>
