<?php
require_once("../../class/Cabecalho.class.php");
require_once("Banner_Superior.class.php");
$obj = new Banner_Superior();
$caminho_projeto = Util::caminho_projeto();



//	VERIFICO SE PARA EFETUAR O CADASTRO
/*if(isset($_POST[btn_cadastrar]))
{
	$obj->cadastrar();
}*/


if(isset($_POST[btn_cadastrar]))
{
	$obj->alterar(base64_decode($_POST[id]));
}
else
{
	$row = $obj->get_dados_tabela(base64_decode($_GET[id]));
}
//$row = $_POST;


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="language" content="pt-br" /> 
<link href="../estilo/estilo.css" rel="stylesheet" type="text/css" />



<!------------------------------------------------------------------------------------------------------------------->
<!--	JSCRIPT	-->
<!------------------------------------------------------------------------------------------------------------------->
<?php
	$obj_jquery = new Biblioteca_Jquery();	
	
	$obj_jquery->biblioteca_masked();
	
	//	CRIO AS MASCARAS
	$obj_jquery->mascara_campos($obj->campos());	
	
	//	CRIO OS JAVASCRIPT PARA OS CAMPOS OBRIGATORIOS
	//Util::campo_obrigatorio_js($obj->campos());
	
	//	VALIDADOR DO FORMULARIO
	$obj_jquery->biblioteca_ketchup();
	$obj_jquery->ketchup("form1");
?>





<title>Administração do site</title>
</head>

<body>

	<!-- Começa -->
<div id="geral">
	<!-- Div para topo -->
	<div id="topo"></div>
    
	<!-- Div para linha no bg -->
	<div id="linhaMenu">
		<div id="cabecalho">Gerenciando </div>
		<div id="sair"><a href="../logout.php"><img src="../img/sair.png" border="0" /></a> </div>
	</div>
    
	<!-- Div conteúdo -->
	<div id="conteudo">

	  
      <!---------------------------------------------------------------------------------------------------------------->
      <!-- Div menu lateral -->
      <!---------------------------------------------------------------------------------------------------------------->
		<div id="menu_left">
        	<div class="cabecalho_menu_left"></div>
            	<div id="menu_left_dentro">
                    <ul>
                        <li><a href="../inicial.php">Home</a></li>
                        <li><a href="index.php">Listar</a></li>
                        <li><a href="cadastro.php">Cadastrar</a></li>    
                    </ul>
               </div>	                	
		 </div>
         
         
         
          <!-- Div Miolo -->         
         <div id="miolo">
         	
            <!-- Navegação -->
            <div id="navegacao">
            	<a href="../inicial.php">Home</a><a href="index.php"></a> 
            </div>
            
            <!-- Cabeçalho -->
            <div class="cabecalho_miolo">
            	 Cadastro
            </div>
            
            
            
            
            <!---------------------------------------------------------------------------------------------------------------->
            <!-- Dentro Miolo -->
            <!---------------------------------------------------------------------------------------------------------------->
            <div id="dentro_miolo">
            
            	
                
            
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" name="form1" id="form1" method="post" enctype="multipart/form-data" onSubmit='return valida_campos_formulario()'>
                    
                 
                    
                   
                    
                    <div class="div_campos_left">
                        <div class="nome_campos">Título:</div>
                        <input type="text" name="titulo" id="titulo" value="<?php Util::imprime($row[titulo]); ?>" size="35" class="validate(required)" />
                    </div>
                    
                  
                    <div class="div_campos_right">
                        <div class="nome_campos">Url de destino:</div>
                        <input name="url_destino" id="url_destino" type="text" value="<?php Util::imprime($row[url_destino]); ?>" size="35" />
                    </div>
                     
                      
                    <div id="quebra"></div> 
<!--
                    
                    <div class="div_campos_left">
                        <div class="nome_campos">Destino da Url:</div>
                        <select name="target" id="target" >
                        	<option value="">Selecione</option>
                            <option value=" ">Mesma página</option>
                            <option value="_blank">Nova página</option>
                        </select>
                    </div>
                    
                    -->

<div id="quebra"></div>
                  <div id="quebra"></div>
                    <div style="margin:0 0 10px 0; display:block; width:100%;">
                    
                    <img src="<?php echo $caminho_projeto;?>uploads/<?php Util::imprime($row[imagem])?>" style="width:100%;" />
                    	<div class="nome_campos"><br />Alterar Imagem:</div>
                        <input name="imagem" id="imagem" type="file" class="validate(required)" />
                        <div class="comentario_campos">A imagem deve ser de 1080px de largura por 561px de altura.</div>
                    </div>
                    
                    
                    <div id="quebra"></div>
                    <div id="quebra"></div>
                   
                   <!----------------------------------------------------------------------->
                   <!--	ACOES DO FORMULARIO	-->
                   <!----------------------------------------------------------------------->
                   <input name="btn_cadastrar" type="submit" value="Cadastrar" class="class_btn_submit"/>
                   
                  
                
                  <input name="id" id="id" type="hidden" value="<?php echo $_GET[id]; ?>"/>
                </form>
            <!-- btn voltar -->
                <div id="btn_voltar">
                	<a href="../inicial.php"><img src="../img/voltar.png" border="0" /></a>
                </div>
                
			</div>
         
         </div>	
	</div>
</div>
</body>

</html>
