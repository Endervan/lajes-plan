<?php   

//Incluir a classe responsavel em redimencionar a imagem em tempo real.
include('class/m2brimagem.class.php');   

/*
* redimensionaImg()
* @param $arquivo: Caminho da imagem
* @param $largura: nova largura da imagem 
* @param $altura: nova altura da imagem 
* @param $tipo: tipo de corte na imagem [crop,fill,''] 
*/

//Recebe os paramentros na chamada da p�gina redimensionar_img
$arquivo    = $_GET['arquivo'];   
$largura    = $_GET['largura'];   
$altura     = $_GET['altura'];
$tipo     	= $_GET['tipo'];

//Execulta o redirecionamento da imagem   
$oImg = new m2brimagem($arquivo);   
$valida = $oImg->valida();   
if ($valida == 'OK') {   
    $oImg->redimensiona($largura,$altura,$tipo);
    $oImg->grava();
} else {   
    die($valida);
}   
exit;   
?>