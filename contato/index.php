<?php
	require_once("../class/Include.class.php"); 
	$obj_site = new Site();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<?php include("../includes/head.php") ?>

<meta name="description" content="<?php echo $description?>" />
<meta name="keywords" content="lajesplan, sua obra" />
<title><?php echo $title ?></title>


</head>

<body class="bg-interna">
    <div id="corpo">
    <!--Topo-->
    	<div id="topo">
			<?php include("../includes/topo.php") ?>
        </div>
    
     <!--Banner-->
     <div id="banner-interna">
   	  <?php include("../includes/onde-estou.php"); ?>
     </div>
     <!--Corpo-->
        <div id="conteiner-interna">            
			<div id="coluna1-contato">
            <?php
                
                    //	VERIFICO SE E PARA ENVIAR O EMAIL
                    if(isset($_POST[nome]))
                    {
						$destinatario = 'lajesplan@lajesplan.com.br';
						$nome_remetente =($_POST[nome]);
						$email =($_POST[email]);
						$mensagem =(nl2br($_POST[mensagem]));
						$telefone =($_POST[fone]);
						$assunto = "Contato pelo site ".$_SERVER[SERVER_NAME]." - ".$nome_remetente;
						
						$texto_mensagem = "
							  Nome: $nome_remetente <br />
							  Telefone: $telefone <br />
							  Email: $email <br />
							  Mensagem:	<br />
							  ".(nl2br($_POST[mensagem]))."
							  ";
					
						
						//	ARMAZENO OS DADOS NO BANCO
						$data = date("Ymd");
						$hora = date("H:i:s");
						
					
				
                        Util::envia_email('lajesplan@lajesplan.com.br', utf8_decode($_POST[assunto]), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
                        Util::envia_email("junior@homewebbrasil.com.br", utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email);	
                        Util::envia_email("angela.homeweb@gmail.com", utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email); 
                        Util::envia_email("marciomas@gmail.com", utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email); 
						Util::script_msg("Obrigado por entrar em contato.");
                    }
                
                ?>
            <h2>ENTRE EM CONTATO</h2>
            <?php
				$result_empresa = $obj_site->select_unico("tb_empresas","idempresa",7);// Busca o conteudo referente ao id 7 select_unico($nome_tabela, $id_tabela, $id)
			?>
            <p><?php Util::imprime($result_empresa[descricao])?></p>
            <form id="form-contato" name="form-contato" method="post" action="">
                <p>
                    <label for="nome">Nome:</label>
                    <input type="text" name="nome" id="nome" class="campo-formulario" />
                </p>
                <p>
                    <label for="email">E-mail:</label>
                    <input type="text" name="email" id="email" class="campo-formulario" />
                </p>
                <p>
                    <label for="fone">Telefone:</label>
                    <input type="text" name="fone" id="fone" class="campo-formulario" />
                </p>
                <p>
                    <label for="mensagem">Mensagem:</label>
                    <textarea name="mensagem" id="mensagem" class="campo-formulario-text"></textarea>
                </p>
                <p>
                	<input type="submit" name="btn-enviar" id="btn-enviar" value="Enviar" class="btn-form" />
                </p>
            </form>
			</div>
            <div id="coluna2-contato">
                <ul>
                	<li>
                        <img src="<?php echo Util::caminho_projeto() ?>imgs/ico-chat.png" alt="Atendimento On-line " />
                        <a href="#">Atendimento on-line</a>
                    </li>
                    <li>
                        <img src="<?php echo Util::caminho_projeto() ?>imgs/ico-email.png" alt="lajesplan@lajesplan.com.br" />
                        <a href="mailto:lajesplan@lajesplan.com.br">lajesplan@lajesplan.com.br</a>
                    </li>
                    <li>
                        <img src="<?php echo Util::caminho_projeto() ?>imgs/ico-localizacao.png" alt="QI 25 LOTES 42-47 SETOR INDUSTRIAL DE TAGUATINGA NORTE - DF"/>
                        <h4>QI 25 LOTES 42-47 SETOR INDUSTRIAL DE TAGUATINGA NORTE - DF<br /> CEP: 72.135-250</h4>
                    </li>
                </ul>
                <iframe width="480" height="280" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=lajesplan&amp;aq=&amp;sll=-15.811992,-48.11986&amp;sspn=0.071434,0.132093&amp;t=m&amp;ie=UTF8&amp;hq=lajesplan&amp;hnear=&amp;ll=-15.810051,-48.07512&amp;spn=0.004465,0.008256&amp;z=14&amp;iwloc=A&amp;cid=3434515397078159744&amp;output=embed"></iframe>
            </div>
            
            
          <div class="clear"></div>
        </div>
        
       <!--Rodape-->
      <div	id="rodape">
			<?php include("../includes/rodape.php"); ?>
      </div>
    </div>
</body>
</html>