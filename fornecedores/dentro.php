<?php
	require_once("../class/Include.class.php"); 
	$obj_site = new Site();
	
	require_once("../class/Resumo.class.php"); 
	$obj_resumo = new resumo();
	
	/*Recupera os parametros da url*/
	$parametros = explode("-",$_GET[get1]);
	$id = $parametros[0];
	
	$fornecedor = $obj_site->select_unico("tb_fornecedores", "idfornecedor", $id); //Busca o conteudo de acordo com o id passado
	
	$description = $obj_resumo->str_truncate($fornecedor[descricao],150);
	$texto = html_entity_decode($fornecedor[titulo]);
	$texto = strip_tags($texto);
	$texto = stripslashes($texto);
	$texto = utf8_encode($texto);
	
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<?php include("../includes/head.php") ?>
<?php $title = $texto.' - Lajesplan :: Fornecedores'; ?>
<meta name="description" content="<?php echo $description?>" />
<meta name="keywords" content="lajesplan, sua obra" />
<title><?php echo $title ?></title>

<!--Troca a foto que esta sendo visualizada no slider-->
<script type="text/javascript"> 
function mudaFoto(id)
{
    var elemento = "#container-infos-galeria-grande-"+id;
    $("#container-infos-galeria-grande div").stop(true, true).fadeOut(500).hide(500);
    $(elemento).stop(true, true).fadeIn(1000).show(500);
}
</script>
</head>

<body class="bg-interna">
    <div id="corpo">
    <!--Topo-->
    	<div id="topo">
			<?php include("../includes/topo.php") ?>
        </div>
    
     <!--Banner-->
     <div id="banner-interna">
   	   <div class="titulo-pagina"><h1><?php echo $local_pagina; ?></h1></div>
       <div class="onde-estou">
        	<a href="<?php echo Util::caminho_projeto();?>">home</a> <span>></span> <a href="<?php echo Util::caminho_projeto(); echo $local_pagina;?>"> <?php echo $local_pagina; ?></a> <span>> <?php Util::imprime($fornecedor[titulo]);?></span>
        </div>
     </div>
     <!--Corpo-->
        <div id="conteiner-interna">    
            <h2><?php Util::imprime($fornecedor[titulo]);?></h2>  
              <?php
			  		if(!empty($fornecedor[imagem_capa]) and file_exists("../uploads/".$fornecedor[imagem_capa])){
						$img = Util::caminho_projeto()."uploads/".$fornecedor[imagem_capa];
					}else{
						$img = Util::caminho_projeto()."uploads/sem-imagem.jpg";
					}
			  ?> 
              <div class="thumb-img">
                  <a href="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $fornecedor[imagem_capa] ?>" title="<?php Util::imprime($fornecedor[titulo])?>" class="vlightbox1"> 
                    <img src="<?php echo $img?>" alt="<?php Util::imprime($fornecedor[titulo])?>" />  
                  </a>
              </div>
            <p>
            	<ul>
                	<?php
						if(!empty($fornecedor[email])){
					?>
                	<li>E-mail: <a href="mailto:<?php Util::imprime($fornecedor[email]);?>"><?php Util::imprime($fornecedor[email]) ?></a></li>
                    <?php
						}
					?>
                    <?php
						if(!empty($fornecedor[url_site])){
						
					?>
                	<li>Site: <a href="<?php Util::imprime($fornecedor[url_site]);?>" target="_blank"><?php Util::imprime($fornecedor[url_site]) ?></a></li>
                    <?php
						}
					?>
                    <?php
						if(!empty($fornecedor[telefone])){
						
					?>
                	<li>Telefone: <?php Util::imprime($fornecedor[telefone]) ?></li>
                    <?php
						}
					?>
                </ul>
            </p>
               
           <div class="clear"></div>
        </div>
        
       <!--Rodape-->
      <div	id="rodape">
			<?php include("../includes/rodape.php"); ?>
      </div>
    </div>
</body>
</html>