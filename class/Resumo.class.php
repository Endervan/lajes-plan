<?php
	
	define("BEFORE", 0);
	define("AFTER", 1);
	
	class resumo {


		public function str_truncate($str, $length, $rep=BEFORE,$limi=NULL){
			
			//corrigir um bug
			$str = strip_tags($str);
			$str  = trim($str);
			
			if(isset($limi)){$limi = '...';}else{$limi = '';}
			
			if(strlen($str)<=$length) return $str;
		
		
			if($rep == BEFORE) $oc = strrpos(substr($str,0,$length),' ');
		
			if($rep == AFTER)  $oc = strpos(substr($str,$length),' ') + $length;
		
		
		
			$string = substr($str, 0, $oc);
		
			if (strlen($str)>$length) $string = $string.$limi;
		
			return $string;
		
		}
	}
?>