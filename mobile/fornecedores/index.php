<?php
require_once("../../class/Include.class.php"); 
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
  <?php require_once('../includes/head.php'); ?>

</head>

<body>


  <?php require_once('../includes/topo.php'); ?>


  <!-- bg, caminho -->
  <div class="container bg-fornecedores onde-vc-esta">
    <div class="row">
      <div class="col-xs-12 top100">
        <ol class="breadcrumb">
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile">Home</a></li>
          <li class="active">Fornecedores</li>
        </ol>
      </div>
    </div>
  </div>
  <!-- bg, caminho -->


<!-- pesquisa -->
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      
    </div>
  </div>
</div>
<!-- pesquisa -->



<!-- listagem -->
<div class="container top25">
  <div class="row">
    <div class="col-xs-12">
      
      <?php
          $result = $obj_site->select("tb_fornecedores");
          if(mysql_num_rows($result) > 0){
            while($row = mysql_fetch_array($result)){
              ?>
              <div class="col-xs-12 lista-fornecedores bottom40">
                  <div class="col-xs-4">
                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem_capa]) ?>" alt="" class="input100 linha-azul">
                  </div>
                  <span class="col-xs-8">
                    <p><?php Util::imprime($row[titulo]) ?></p>
                    <p><strong>Email:</strong> <?php Util::imprime($row[email]) ?></p>
                    <p><strong>Site: </strong><?php Util::imprime($row[url_site]) ?></p>
                    <p><strong>Telefone:</strong> <?php Util::imprime($row[telefone]) ?></p>
                  </span>
                  
              </div>
              <?php
            }
          }
          ?>

    </div>
  </div>
</div>
<!-- listagem -->


<?php require_once('../includes/rodape.php'); ?>

</body>
</html>