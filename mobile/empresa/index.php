<?php
require_once("../../class/Include.class.php"); 
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<body>


	<?php require_once('../includes/topo.php'); ?>


  <!-- bg, caminho -->
  <div class="container bg-empresa onde-vc-esta">
    <div class="row">
      <div class="col-xs-12 top100">
        <ol class="breadcrumb">
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile">Home</a></li>
          <li class="active">Empresa</li>
        </ol>
      </div>
    </div>
  </div>
  <!-- bg, caminho -->



<div class="container">
  <div class="row">
    <div class="col-xs-12">
       <?php $dados = $obj_site->select_unico("tb_empresas", "idempresa", 4) ?>
       <img src="../imgs/img-empresa.jpg" alt="" class="top25 bottom15">
       <p><?php Util::imprime($dados[descricao]) ?></p>
        
        <h1 class="top30">Missão, Visão e Valores</h1>
       <?php $dados = $obj_site->select_unico("tb_empresas", "idempresa", 5) ?>
       <p><?php Util::imprime($dados[descricao]) ?></p>
    </div>
  </div>
</div>


<?php require_once('../includes/rodape.php'); ?>

</body>
</html>