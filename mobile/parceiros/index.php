<?php
require_once("../../class/Include.class.php"); 
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
  <?php require_once('../includes/head.php'); ?>

</head>

<body>


  <?php require_once('../includes/topo.php'); ?>


  <!-- bg, caminho -->
  <div class="container bg-parceiros onde-vc-esta">
    <div class="row">
      <div class="col-xs-12 top100">
        <ol class="breadcrumb">
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile">Home</a></li>
          <li class="active">Parceiros</li>
        </ol>
      </div>
    </div>
  </div>
  <!-- bg, caminho -->




<!-- listagem -->
<div class="container top25">
  <div class="row">
    <div class="col-xs-12">
      
      <?php
          $result = $obj_site->select("tb_parceiros");
          if(mysql_num_rows($result) > 0){
            while($row = mysql_fetch_array($result)){
              ?>
              <div class="col-xs-4 lista-produtos">
                  <div class="lista-produto-img">
                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="input100">
                  </div>
                  <p><?php Util::imprime($row[titulo]) ?></p>
              </div>
              <?php
            }
          }
          ?>

    </div>
  </div>
</div>
<!-- listagem -->


<?php require_once('../includes/rodape.php'); ?>

</body>
</html>