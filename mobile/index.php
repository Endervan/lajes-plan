<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>
  
   <!-- ---- LAYER SLIDER ---- -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $("#carousel-gallery").touchCarousel({
        itemsPerPage: 1,
        scrollbar: true,
        scrollbarAutoHide: true,
        scrollbarTheme: "dark",
        pagingNav: false,
        snapToItems: true,
        scrollToLast: false,
        useWebkit3d: true,
        loopItems: true
      });
    });
  </script>
  <!-- XXXX LAYER SLIDER XXXX -->

</head>

<body>



  <?php require_once('./includes/topo.php'); ?>



  <!-- banner e slider  home -->
  <div class="container">
    <div class="row">
      <div class="bg-home"></div>
      <div class="col-xs-12 caroucel-personalizado">

        <div id="carousel-example-generic " class="carousel slide slider-index" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators navs-personalizados">
          <?php
          $result = $obj_site->select("tb_portfolio", "AND imagem <> '' ORDER BY rand() LIMIT 6");
          if (mysql_num_rows($result) > 0) {
            $i = 0;
            while($row = mysql_fetch_array($result)){

                            //  selecionador da classe active
              if($i == 0){
                $active = 'active';
              }else{
                $active = '';
              }

              ?>

              <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php echo $active ?>"></li>

              <?php
              $imagens[] = $row;
              $i++;
            }
          }
          ?>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner navs-personalizados" role="listbox">
            
            <?php
            $i = 0;
            if (count($imagens) > 0) {
              foreach($imagens as $imagem){

                              //  selecionador da classe active
                if($i == 0){
                  $active = 'active';
                }else{
                  $active = '';
                }

                ?>
                <div class="item <?php echo $active ?>">
                  <a href="./portfolio/<?php echo $obj_site->url($imagem[0], $imagem[titulo]) ?>">
                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($imagem[imagem]) ?>" alt="" class="imput100">
                    
                    <div class="carousel-caption">
                      <h1><?php Util::imprime($imagem[titulo]) ?></h1>
                      <a href="">SAIBA MAIS</a>
                    </div>
                  </a>
                </div>
                <?php
                $i++;
              }
            }
            ?>

          </div>

        </div>

      </div>
    </div>
  </div>
  <!-- banner e slider home -->


  <!-- empresa -->
  <div class="container">
    <div class="row top60">
    <div class="col-xs-12 empresa-index">
      <h1>A EMPRESA</h1>
      <img src="./imgs/img-empresa.jpg" alt="" class="input100 top10 bottom10">
      <?php $dados = $obj_site->select_unico("tb_empresas", "idempresa", 4) ?>
      <p><?php Util::imprime($dados[descricao], 600) ?></p>
      <div class="text-right top15 bottom65">
        <a href="./empresa/" class="btn btn-grande">
          CONHEÇA MAIS A EMPRESA
        </a>
      </div>
    </div>
    </div>
  </div>
  <!-- empresa -->


  <!-- contato, dicas -->
<div class="container">
  <div class="row index-contato-dicas">
    <div class="col-xs-6">
      <h1>Entre em contato</h1>
      <?php $dados = $obj_site->select_unico("tb_empresas", "idempresa", 7) ?>
      <p><?php Util::imprime($dados[descricao], 600) ?></p>
      <a href="./contatos/" class="btn btn-medio">
        entre em contato
      </a>
    </div>

    <div class="col-xs-6">
      <h1>Dicas</h1>
      <?php $dados = $obj_site->select_unico("tb_empresas", "idempresa", 9) ?>
      <p><?php Util::imprime($dados[descricao], 600) ?></p>
      <a href="./dicas/" class="btn btn-medio">
        veja mais dicas
      </a>
    </div>

  </div>
</div>
  <!-- contato, dicas -->


<!-- fornecedores -->
<div class="container top35">
  <div class="row">
  <div class="col-xs-12">
    <h1>Fornecedores</h1>
    <?php $dados = $obj_site->select_unico("tb_empresas", "idempresa", 8) ?>
    <p><?php Util::imprime($dados[descricao], 600) ?></p>
    
    <div id="carousel-gallery" class="touchcarousel  black-and-white top15">

        <ul class="touchcarousel-container">


          <?php
          $result = $obj_site->select("tb_fornecedores", "ORDER BY rand() LIMIT 10");
          if(mysql_num_rows($result) > 0){
            while($row = mysql_fetch_array($result)){
              ?>
              <li class="touchcarousel-item col-xs-3">
                  <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem_capa]) ?>" alt="" class="input100">
              </li>
              <?php
            }
          }
          ?>
        </ul>

      </div>

  </div>
  </div>
</div>
<!-- fornecedores -->







  <?php require_once('./includes/rodape.php'); ?>


</body>

</html>
