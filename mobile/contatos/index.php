<?php
require_once("../../class/Include.class.php"); 
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
  <?php require_once('../includes/head.php'); ?>




</head>

<body>


  <?php require_once('../includes/topo.php'); ?>



  
  <!-- bg, caminho -->
  <div class="container bg-parceiros onde-vc-esta">
    <div class="row">
      <div class="col-xs-12 top100">
        <ol class="breadcrumb">
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile">Home</a></li>
          <li class="active">Contato</li>
        </ol>
      </div>
    </div>
  </div>
  <!-- bg, caminho -->




<!-- ligue -->
<div class="container top25">
  <div class="row">
    <div class="col-xs-12">
      <h1>ENTRE EM CONTATO</h1>
      <?php $dados = $obj_site->select_unico("tb_empresas", "idempresa", 7) ?>
       <p><?php Util::imprime($dados[descricao]) ?></p>
    </div>



    <div class="col-xs-6 top15">
      <div class="btn-contato-ligar">
        <span class="left5 right5">(61) 3568.1040</span>
        <a href="tel:+556135681040" class="btn btn-vermelho">CHAMAR</a>
      </div>

      <div class="btn-contato-ligar">
        <span class="left5 right5">(61) 3381.2492</span>
        <a href="tel:+556133812492" class="btn btn-vermelho">CHAMAR</a>
      </div>

      <div class="btn-contato-ligar">
        <span class="left5 right5">(61) 3381.1540</span>
        <a href="tel:+556133811540" class="btn btn-vermelho">CHAMAR</a>
      </div>
    </div>

    <div class="col-xs-6 top15">
      <p class="text-left">
        QI 25 LOTES 42-47<br /> SETOR INDUSTRIAL DE TAGUATINGA NORTE- DF<br /> CEP 72.135-250
      </p>
      <div class="top10 text-center">
        <a href="https://www.google.com/maps?ll=-15.810051,-48.07512&z=14&t=m&hl=pt-BR&gl=US&mapclient=embed" class="btn btn-azul">COMO CHEGAR</a>
      </div>
      
    </div>

  </div>
</div>
<!-- ligue -->






<!-- formulario -->
<div class="container">
  <div class="row titulo-form-contato">
    <?php
                    //  VERIFICO SE E PARA ENVIAR O EMAIL
    if(isset($_POST[btn_contato]))
    {
      $nome_remetente = Util::trata_dados_formulario($_POST[nome]);
      $email = Util::trata_dados_formulario($_POST[email]);
      $assunto = Util::trata_dados_formulario($_POST[assunto]);
      $telefone = Util::trata_dados_formulario($_POST[telefone]);
      $mensagem = Util::trata_dados_formulario(nl2br($_POST[mensagem]));
      $texto_mensagem = "
      Nome: $nome_remetente <br />
      Assunto: $assunto <br />
      Telefone: $telefone <br />
      Email: $email <br />
      Mensagem: <br />
      $mensagem
      ";
      Util::envia_email($config[email], "CONTATO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
      Util::envia_email($config[email_copia], "CONTATO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
      Util::script_msg("Obrigado por entrar em contato.");
      unset($_POST);
    }
    ?>

    <div class="col-xs-12">
      <form class="form-inline FormContato top20 pbottom20" role="form" method="post">

        <div class="row">
          <div class="col-xs-6 form-group ">
            <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
            <input type="text" name="nome" class="form-control fundo-form input100" placeholder="">
          </div>
          <div class="col-xs-6 form-group">
            <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
            <input type="text" name="email" class="form-control fundo-form input100" placeholder="">
          </div>
        </div>

        <div class="row">
          <div class="col-xs-6 top20 form-group">
            <label class="glyphicon glyphicon-earphone"><span>Telefone</span></label>
            <input type="text" name="telefone" class="form-control fundo-form input100" placeholder="">
          </div>
          <div class="col-xs-6 top20 form-group">
            <label class="glyphicon glyphicon-star"> <span>Assunto</span></label>
            <input type="text" name="assunto" class="form-control fundo-form input100" placeholder="">
          </div>

        </div>

        <div class="row">
          <div class="col-xs-12 top20 form-group">
            <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
            <textarea name="mensagem" id="" cols="30" rows="10" class="form-control  fundo-form input100" placeholder=""></textarea>
          </div>

        </div>

        <div class="clearfix"></div>

        <div class="text-right right15 top30">
          <button type="submit" class="btn btn-azul" name="btn_contato">
            ENVIAR
          </button>
        </div>


      </form>
    </div>

  </div>
</div>
<!-- formulario -->



<div class="container top30">
  <div class="row">
    <div class="col-xs-12">
      <p class="bottom20">QI 25 LOTES 42-47 SETOR INDUSTRIAL DE TAGUATINGA NORTE- DF<br /> CEP 72.135-250</p>
      <iframe width="100%" height="415" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=lajesplan&amp;aq=&amp;sll=-15.811992,-48.11986&amp;sspn=0.071434,0.132093&amp;t=m&amp;ie=UTF8&amp;hq=lajesplan&amp;hnear=&amp;ll=-15.810051,-48.07512&amp;spn=0.004465,0.008256&amp;z=14&amp;iwloc=A&amp;cid=3434515397078159744&amp;output=embed"></iframe>
    </div>
  </div>
</div>















<?php require_once('../includes/rodape.php'); ?>

</body>
</html>







<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      tipo_pessoa: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>










<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
});
</script>



