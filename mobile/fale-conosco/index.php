<?php
require_once("../../class/Include.class.php"); 
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
	<?php require_once('../includes/head.php'); ?>

</head>


</head>

<body>


	<?php require_once('../includes/topo.php'); ?>

  <!-- BG-contatos -->
  <div class="container">
    <div class="row">
      <div class="bg-empresa topo1">
        <div id="carousel-example-generic" class="carousel slide posicao-carroucel" data-ride="carousel">
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/bg-nossos-contatos.jpg"alt="...">
              <div class="carousel-caption">
              </div>
            </div>
          </div>
        </div>
      </div>  
      <!--BG- contatos -->
    </div>
  </div>
  


  <!-- descricao FALE CONOSCO -->
  <div class="container">
    <div class="row bottom15">
      <!-- barra internas -->
      <div class="posicao-barra-transparente">
        <ol class="breadcrumb col-xs-6 col-xs-offset-3 padronizar-barra">
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile">Home<i class="fa fa-angle-right"></i></a></li>
          <li class="active">FALE CONOSCO</li>
        </ol>  
      </div>
      <!-- barra internas -->
      <!-- contatos geral -->
      <div class="col-xs-12 top20">
        <div class="descricao-fale-conosco">
          <div class="col-xs-8 text-center">
            <h2>Asa Sul: <span>61 3346-0101</span></h2>
          </div>
          <div class="col-xs-4">
            <a href="tel:33333333" class="btn btn-fale-conosco" title="CHAMAR">
              CHAMAR
            </a>
          </div>
        </div>
      </div>

      <!-- contatos geral -->
      <div class="col-xs-12 top20">
        <div class="descricao-fale-conosco">
          <div class="col-xs-8 text-center">
            <h2>Asa Norte: <span>61 3346-0101</span></h2>
          </div>
          <div class="col-xs-4">
            <a href="tel:33333333" class="btn btn-fale-conosco" title="CHAMAR">
              CHAMAR
            </a>
          </div>
        </div>
      </div>

    </div>
  </div>

  <!-- formulario fale conosco -->
  <div class="container bottom80">
    <div class="row">
      <div class="col-xs-12 text-center descer-descricao">
        <div class="formulario-fale">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/titulo-enviar-email.png"alt=""> 
        </div>
      </div>

      <div class="col-xs-12">
        <div class="formulario-fale ">
          <form class="form-inline FormContato pbottom20" role="form" method="post">

            <div class="row">
              <div class="col-xs-10 col-xs-offset-1 top20 form-group ">
                <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                <input type="text" name="nome" class="form-control   input100" placeholder="">
              </div>
            </div>


            <div class="row">
              <div class="col-xs-10 col-xs-offset-1 top5 form-group">
                <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                <input type="text" name="email" class="form-control    input100" placeholder="">
              </div>

            </div>

            <div class="row">
              <div class="col-xs-10 col-xs-offset-1 top5 form-group">
                <label class="glyphicon glyphicon-earphone"><span>Telefone</span></label>
                <input type="text" name="telefone" class="form-control   input100" placeholder="">
              </div>
            </div>


            <div class="row">
              <div class="col-xs-10 col-xs-offset-1 top5 form-group">
                <label class="glyphicon glyphicon-star"> <span>Assunto</span></label>
                <input type="text" name="assunto" class="form-control    input100" placeholder="">
              </div>
            </div>

            

            <div class="row">
              <div class="col-xs-10 col-xs-offset-1 top5 form-group">
                <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
                <textarea name="mensagem" id="" cols="30" rows="10" class="form-control    input100" placeholder=""></textarea>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="text-right left10 top30">
              <a href="">
                <!-- Standard button -->
                <button type="button" class="btn btn-default btn-fale-conosco1 right top30">Enviar</button>
              </a>

            </div>


          </form>

        </div>
        <!-- sombra fale conosco -->
        <div class="sombra-fale-conosco"></div> 
        <!-- sombra fale conosco --> 
      </div>
    </div>
    
    <!-- formulario fale conosco -->

  </div>
  


  <?php require_once('../includes/rodape.php'); ?>

</body>
</html>



<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>
