<?php
require_once("../../class/Include.class.php"); 
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
  <?php require_once('../includes/head.php'); ?>

</head>

<body>


  <?php require_once('../includes/topo.php'); ?>


  <!-- bg, caminho -->
  <div class="container bg-produtos onde-vc-esta">
    <div class="row">
      <div class="col-xs-12 top100">
        <ol class="breadcrumb">
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile">Home</a></li>
          <li class="active">Produtos</li>
        </ol>
      </div>
    </div>
  </div>
  <!-- bg, caminho -->


<!-- pesquisa -->
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <form action="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" class="top20 bottom20 form-search form-inline FormBusca bv-form" method="post" novalidate="novalidate"><button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
          <button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
          <div class="input-group form-group has-feedback" style="width: 100%;">
              <input type="text" name="busca_produtos" class="form-control search-query input-busca-topo" placeholder="PESQUISAR PRODUTOS" style="border: none;" data-bv-field="busca_topo"><i class="form-control-feedback bv-no-label" data-bv-icon-for="busca_topo" style="display: none;" data-original-title="" title=""></i><i class="form-control-feedback bv-no-label" data-bv-icon-for="busca_dica" style="display: none;" data-original-title="" title=""></i> 
              <span class="input-group-btn">
                  <button type="submit" class="btn btn-default glyphicon glyphicon-search input-busca-produto" style="margin-top: -1px; margin-left: -4px;">
              </button>
              </span>
              <small class="help-block" data-bv-validator="notEmpty" data-bv-for="busca_dica" data-bv-result="NOT_VALIDATED" style="display: none;">Por favor, preêncha esse campo.</small>
          <small class="help-block" data-bv-validator="notEmpty" data-bv-for="busca_topo" data-bv-result="NOT_VALIDATED" style="display: none;">Por favor, preêncha esse campo.</small></div>
      </form>
    </div>
  </div>
</div>
<!-- pesquisa -->



<!-- listagem -->
<div class="container top25">
  <div class="row">
    <div class="col-xs-12">
      
      <?php

          if (isset($_POST[busca_produtos])) {
            $complemento = "AND titulo LIKE '%$_POST[busca_produtos]%'";
          }

          $result = $obj_site->select("tb_produtos", $complemento);
          if(mysql_num_rows($result) == 0){
            echo '<p class="bg-warning">Nenhum produto encontrado.</p>';
          }else{
            while($row = mysql_fetch_array($result)){
              ?>
              <div class="col-xs-4 lista-produtos">
                <a href="<?php echo $obj_site->url($row[0], $row[titulo]) ?>">
                  <div class="lista-produto-img">
                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem_capa]) ?>" alt="" class="input100">
                  </div>
                  <p><?php Util::imprime($row[titulo]) ?></p>
                </a>
              </div>
              <?php
            }
          }
          ?>

    </div>
  </div>
</div>
<!-- listagem -->


<?php require_once('../includes/rodape.php'); ?>

</body>
</html>