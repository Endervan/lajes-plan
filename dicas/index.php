<?php
	require_once("../class/Include.class.php"); 
	$obj_site = new Site();
	

	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<?php include("../includes/head.php") ?>

<meta name="description" content="<?php echo $description?>" />
<meta name="keywords" content="lajesplan, sua obra" />
<title><?php echo $title ?></title>
</head>

<body class="bg-interna">
    <div id="corpo">
    <!--Topo-->
    	<div id="topo">
			<?php include("../includes/topo.php") ?>
        </div>
    
     <!--Banner-->
     <div id="banner-interna">
   	    <?php include("../includes/onde-estou.php"); ?> 
     </div>
     <!--Corpo-->
        <div id="conteiner-interna">    
               <ul id="lista-portfolio">
               <?php
			   		$result = $obj_site->select("tb_dicas"," $complemento ORDER BY titulo ASC");
				while($arr = mysql_fetch_array($result)){
			   ?>
               
               	<li>
                    <p>
                    	<?php
							if(!empty($arr[imagem]) and file_exists("../uploads/".$arr[imagem])){
						?>
                    	<div class="thumb-img">
                            <a href="<?php echo Util::caminho_projeto()?>dicas/<?php echo $obj_site->url($arr[0],$arr[titulo]) ?>" title="<?php Util::imprime($arr[titulo])?>"></a>
                            <img src="<?php echo Util::caminho_projeto()?>uploads/tumb_<?php Util::imprime($arr[imagem])?>" alt="<?php Util::imprime($arr[titulo])?>" />
                        </div>
                        <?php
							}
						?>
                        
                    	<h2>
                        	<a href="<?php echo Util::caminho_projeto()?>dicas/<?php echo $obj_site->url($arr[0],$arr[titulo]) ?>" title="<?php Util::imprime($arr[titulo])?>" class="link_dicas">
								<?php Util::imprime($arr[titulo])?>
                            </a>
                        </h2>
                       <a href="<?php echo Util::caminho_projeto()?>dicas/<?php echo $obj_site->url($arr[0],$arr[titulo]) ?>" title="<?php Util::imprime($arr[titulo])?>">
                    	<?php Util::imprime($arr[descricao],200)?>...
                        </a>
                        
                    </p>
                </li>
                <?php
				}
				?>
               </ul>
               
           <div class="clear"></div>
        </div>
        
       <!--Rodape-->
      <div	id="rodape">
			<?php include("../includes/rodape.php"); ?>
      </div>
    </div>
</body>
</html>