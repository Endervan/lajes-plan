<?php
	require_once("../class/Include.class.php"); 
	$obj_site = new Site();
	
	require_once("../class/Resumo.class.php"); 
	$obj_resumo = new resumo();
	
	/*Recupera os parametros da url*/
	$parametros = explode("-",$_GET[get1]);
	$id = $parametros[0];
	
	$dica = $obj_site->select_unico("tb_dicas", "iddica", $id); //Busca o conteudo de acordo com o id passado
	
	
	$description = $obj_resumo->str_truncate($dica[descricao],150);
	$texto = html_entity_decode($dica[titulo]);
	$texto = strip_tags($texto);
	$texto = stripslashes($texto);
	$texto = utf8_encode($texto);
	
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<?php include("../includes/head.php") ?>
<?php $title = $texto.' - Lajesplan :: Dicas'; ?>
<meta name="description" content="<?php echo $description?>" />
<meta name="keywords" content="lajesplan, sua obra" />
<title><?php echo $title ?></title>

</head>

<body class="bg-interna">
    <div id="corpo">
    <!--Topo-->
    	<div id="topo">
			<?php include("../includes/topo.php") ?>
        </div>
    
     <!--Banner-->
     <div id="banner-interna">
   	   <div class="titulo-pagina"><h1><?php echo $local_pagina; ?></h1></div>
       <div class="onde-estou">
        	<a href="<?php echo Util::caminho_projeto();?>">home</a> <span>></span> <a href="<?php echo Util::caminho_projeto();?>dicas"> <?php echo $local_pagina; ?></a> <span>> <strong><?php Util::imprime($dica[titulo]);?></strong></span>
        </div>
     </div>
     <!--Corpo-->
        <div id="conteiner-interna">    
            <h2><?php Util::imprime($dica[titulo]);?></h2>
			<?php
				if(!empty($dica[imagem]) and file_exists("../uploads/".$dica[imagem])){
			?>
            	<a href="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $dica[imagem] ?>" class="vlightbox1">
            	<img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $dica[imagem];?>" alt="<?php Util::imprime($dica[titulo]);?>" style="max-width:400px;" />
                </a>
            <?php
				}
			?>
            	
            <p><?php Util::imprime($dica[descricao]);?></p>
               
           <div class="clear"></div>
        </div>
        
       <!--Rodape-->
      <div	id="rodape">
			<?php include("../includes/rodape.php"); ?>
      </div>
    </div>
</body>
</html>